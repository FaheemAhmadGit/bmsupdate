package com.bellurbis.common;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


import org.springframework.stereotype.Component;

import com.bellurbis.model.BatteryEntity;
import com.bellurbis.model.BellurbissBatteryData;
import com.bellurbis.model.BellurbisUSABatteryData;
import com.bellurbis.model.BellubissGurugramBatteryData;
import com.bellurbis.model.BellurbissIndoreBaatteryData;


@Component
public class CommonUtil {
	
	public static final String time="b.getTime()";
	public static final String date="b.getDate()";
	public static final String orgId="b.getOrgId()";
	public static final String sequeneNumber="b.getSequeneNumber()";
	public static final String bLive="b.isbLive()";
	public static final String latitude="b.getLatitude()";
	public static final String logitude="b.getLogitude()";
	public static final String bin="b.getBin()";
	public static final String batteryStatus="b.getBatteryStatus()";
	public static final String soc="b.getSoc()" ;
	public static final String soh="b.getSoh()";
	public static final String sop="b.getSop()";
	public static final String equivalentCycle="b.getEquivalentCycle()";
	public static final String minimumCellVolatage="b.getMinimumCellVolatage()";
	public static final String maximumCallVolatage="b.getMaximumCallVolatage()";
	public static final String minimumCellTemperature="b.getMinimumCellTemperature()";
	public static final String maximumCellTemperature="b.getMaximumCellTemperature()";
	public static final String current="b.getCurrent()";
	public static final String cycleCapacity="b.getCycleCapacity()";
	public static final String bmsBuildNumber="b.getBmsBuildNumber()";
	public static final String bmsConfigurationVersion="b.getBmsConfigurationVersion()";
	public static final String gpsValidDataStatus="b.getGpsValidDataStatus()";
	public static final String tcuFwversion="b.getTcuFwversion()";
	public static final String tcuAccx="b.getTcuAccx()";
	public static final String tcuAccy="b.getTcuAccy()";
	public static final String tcuAccz="b.getTcuAccz()";
	public static final String warnings="b.getWarnings()";
	public static final String errors="b.getErrors()";
	public static final String internalRunningState="b.getInternalRunningState()";
	public static final String boardTemperature="b.getBoardTemperature()";
	public static final String pduTemperature="b.getPduTemperature()";
	public static final String agingDebugInfoAdvance="b.getAgingDebugInfoAdvance()";
	public static final String agingDebugInfoImpede="b.getAgingDebugInfoImpede()";
	public static final String events="b.getEvents()";
	public static final String networkMode="b.getNetworkMode()";
	public static final String networkInformation="b.getNetworkInformation()";
	public static final String numberOfCells="b.getNumberOfCells()";
	public static final String numberOfTemperatureSensors="b.getNumberOfTemperatureSensors()";
	public static final String cellVoltages="b.getCellVoltages()";
	public static final String id="b.getId()";
	public static final String cellTemperature="b.getCellTemperature()";
	public static final String createdDate="b.getCreatedDate()";
	public static final String tcu_date="b.getTcu_date()";
	public static final String tcu_date_ist="b.getTcu_date_ist()";
	public static final String deviceId="b.getDeviceId()";
	/*
	 * public static final String cycleCapacity public static final String public
	 * static final String public static final String public static final String
	 * public static final String public static final String
	 * 
	 * 
	 */
	
	
	public static String getConstraints(String param)
	{
		return "b.get"+param.substring(0, 1).toUpperCase() + param.substring(1)+"()";
	}
	
	public Date getModifiedTimeinIST(BatteryEntity b) throws ParseException {
		String dd = b.getDate().substring(0, 2);
		String mm = b.getDate().substring(2, 4);
		String yy = b.getDate().substring(4);
		String hh = b.getTime().substring(0, 2);
		String min = b.getTime().substring(2, 4);
		String sec = b.getTime().substring(4);
		String dateFormat = "20" + yy + "-" + mm + "-" + dd + " " + hh + ":" + min + ":" + sec;

		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date startDate;
		return startDate = df.parse(dateFormat);

	}


	public Date getModifiedTimeinIST(String date,String time) throws ParseException {
		
		System.err.println(date);
		System.err.println(time);
		String dd = date.substring(0, 2);
		String mm = date.substring(2, 4);
		String yy = date.substring(4);
		String hh = time.substring(0, 2);
		String min = time.substring(2, 4);
		String sec = time.substring(4);
		String dateFormat = "20" + yy + "-" + mm + "-" + dd + " " + hh + ":" + min + ":" + sec;

		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date startDate;
		return startDate = df.parse(dateFormat);

	}
	public Date convertSimpleDateFormat(Date date) throws ParseException {

		Calendar cal = null;

		cal = Calendar.getInstance(); // creates calendar
		cal.setTime(date); // sets calendar time/date
		cal.add(Calendar.MINUTE, 330); // adds one hour
		Date time = cal.getTime();

		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

		String format = formatter.format(time);

		Date date1 = formatter.parse(format);
		return date1;

	}
	
//
	public Date getModifiedTimeFromTCU(BatteryEntity b) throws ParseException {
		String dd = b.getDate().substring(0, 2);
		String mm = b.getDate().substring(2, 4);
		String yy = b.getDate().substring(4);
		String hh = b.getTime().substring(0, 2);
		String min = b.getTime().substring(2, 4);
		String sec = b.getTime().substring(4);
		String dateFormat = "20" + yy + "-" + mm + "-" + dd + " " + hh + ":" + min + ":" + sec;

		DateFormat df = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
		Date parse = df.parse(dateFormat);

		return df.parse(dateFormat);

	}
	public Date getModifiedTimeFromTCUforRBML(BellubissGurugramBatteryData b) throws ParseException {
		String dd = b.getDate().substring(0, 2);
		String mm = b.getDate().substring(2, 4);
		String yy = b.getDate().substring(4);
		String hh = b.getTime().substring(0, 2);
		String min = b.getTime().substring(2, 4);
		String sec = b.getTime().substring(4);
		String dateFormat = "20" + yy + "-" + mm + "-" + dd + " " + hh + ":" + min + ":" + sec;

		DateFormat df = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
		Date parse = df.parse(dateFormat);

		return df.parse(dateFormat);

	}
	public Date getModifiedTimeFromTCUforExicom(BellurbissBatteryData b) throws ParseException {
		String dd = b.getDate().substring(0, 2);
		String mm = b.getDate().substring(2, 4);
		String yy = b.getDate().substring(4);
		String hh = b.getTime().substring(0, 2);
		String min = b.getTime().substring(2, 4);
		String sec = b.getTime().substring(4);
		String dateFormat = "20" + yy + "-" + mm + "-" + dd + " " + hh + ":" + min + ":" + sec;

		DateFormat df = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
		Date parse = df.parse(dateFormat);

		return df.parse(dateFormat);

	}
	public Date getModifiedTimeFromTCUforRRL(BellurbissIndoreBaatteryData b) throws ParseException {
		String dd = b.getDate().substring(0, 2);
		String mm = b.getDate().substring(2, 4);
		String yy = b.getDate().substring(4);
		String hh = b.getTime().substring(0, 2);
		String min = b.getTime().substring(2, 4);
		String sec = b.getTime().substring(4);
		String dateFormat = "20" + yy + "-" + mm + "-" + dd + " " + hh + ":" + min + ":" + sec;

		DateFormat df = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
		Date parse = df.parse(dateFormat);

		return df.parse(dateFormat);

	}
	public Date getModifiedNewTimeinIST(BatteryEntity b) throws ParseException {
		String dd = b.getTcuNewDate().substring(0, 2);
		String mm = b.getTcuNewDate().substring(2, 4);
		String yy = b.getTcuNewDate().substring(4);
		String hh = b.getTcuNewTime().substring(0, 2);
		String min = b.getTcuNewTime().substring(2, 4);
		String sec = b.getTcuNewTime().substring(4);
		String dateFormat = "20" + yy + "-" + mm + "-" + dd + " " + hh + ":" + min + ":" + sec;

		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date startDate;
		return startDate = df.parse(dateFormat);

	}
	public Date getModifiedNewTimeinISTForExicom(BellurbissBatteryData b) throws ParseException {
		String dd = b.getTcuNewDate().substring(0, 2);
		String mm = b.getTcuNewDate().substring(2, 4);
		String yy = b.getTcuNewDate().substring(4);
		String hh = b.getTcuNewTime().substring(0, 2);
		String min = b.getTcuNewTime().substring(2, 4);
		String sec = b.getTcuNewTime().substring(4);
		String dateFormat = "20" + yy + "-" + mm + "-" + dd + " " + hh + ":" + min + ":" + sec;

		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date startDate;
		return startDate = df.parse(dateFormat);

	}

	public Date getModifiedNewTimeinISTForRRL(BellurbissIndoreBaatteryData b) throws ParseException {
		String dd = b.getTcuNewDate().substring(0, 2);
		String mm = b.getTcuNewDate().substring(2, 4);
		String yy = b.getTcuNewDate().substring(4);
		String hh = b.getTcuNewTime().substring(0, 2);
		String min = b.getTcuNewTime().substring(2, 4);
		String sec = b.getTcuNewTime().substring(4);
		String dateFormat = "20" + yy + "-" + mm + "-" + dd + " " + hh + ":" + min + ":" + sec;

		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date startDate;
		return startDate = df.parse(dateFormat);

	}
	public Date getModifiedNewTimeinISTForRBML(BellubissGurugramBatteryData b) throws ParseException {
		String dd = b.getTcuNewDate().substring(0, 2);
		String mm = b.getTcuNewDate().substring(2, 4);
		String yy = b.getTcuNewDate().substring(4);
		String hh = b.getTcuNewTime().substring(0, 2);
		String min = b.getTcuNewTime().substring(2, 4);
		String sec = b.getTcuNewTime().substring(4);
		String dateFormat = "20" + yy + "-" + mm + "-" + dd + " " + hh + ":" + min + ":" + sec;

		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date startDate;
		return startDate = df.parse(dateFormat);

	}
	public Date getModifiedNewTimeinISTForExicomBounce(BellurbisUSABatteryData b) throws ParseException {
		String dd = b.getTcuNewDate().substring(0, 2);
		String mm = b.getTcuNewDate().substring(2, 4);
		String yy = b.getTcuNewDate().substring(4);
		String hh = b.getTcuNewTime().substring(0, 2);
		String min = b.getTcuNewTime().substring(2, 4);
		String sec = b.getTcuNewTime().substring(4);
		String dateFormat = "20" + yy + "-" + mm + "-" + dd + " " + hh + ":" + min + ":" + sec;

		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date startDate;
		return startDate = df.parse(dateFormat);

	}

}
