package com.bellurbis.entity;

import lombok.Data;
import lombok.ToString;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(schema = "public", name = "t_asset")
@ToString
@Data
public class AssetEntity extends Audit {
	  @Id
	    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "s_asset")
	    @SequenceGenerator(name = "s_asset", schema = "public", sequenceName = "s_asset", allocationSize = 1)
	    private Integer id;


	    @Column(name = "org_id")
	    private int orgId;

	    @Column(name = "imei_no")
	    private Long imeiNo;

	    @Column(name = "search_tags")
	    private String searchTags;

	    @Column
	    private String bin;

	    @Column(name = "asset_type")
	    private String assetType;

	    @Column(name = "asset_catagory")
	    private String assetCatagory;

	    @Column(name = "model_name")
	    private String modelName;


	    @Column
	    private String status;

	    @Column
	    private String descriptions;

	    @Column
	    private String location;

	    @Column
	    private Date lastUpdateDate;

	    @Column(name = "tcu_fw_version")
	    private String tcuFwversion;

	    @Column(name = "bms_build_no")
	    private Integer bmsBuildNumber;

	    @Column(name = "bms_config_file_version")
	    private Integer bmsConfigurationVersion;

	    @Column(name = "model_id")
	    private String modelId;

	    @Column(name = "model_version_id")
	    private Integer modelVersionId;
	    @Column
	    private Float longitude;
	    @Column
	    private Float latitude;

	    @Column
	    private String tcu;

	    @Column
	    private String pdu;

	    @Column
	    private String bms;

	    @Column(name = "mobile_no")
	    private String mobileNo;

	    @Column(name = "sim_no")
	    private String simNo;

	    @Column(name = "dispatch_date")
	    private Date dispatchDate;

	    @Column(name = "partcode")
	    private String partcode;

	    @Column(name = "data01")
	    private String data01;

	    @Column(name = "data02")
	    private String data02;

	    @Column(name = "data03")
	    private String data03;

	    @Column(name = "data04")
	    private Integer data04;

//	    @Column(name = "data05", columnDefinition = "long default 0")
//	    private long data05;

}
