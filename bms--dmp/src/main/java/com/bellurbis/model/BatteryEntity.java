package com.bellurbis.model;

import lombok.Data;

import static javax.persistence.GenerationType.SEQUENCE;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Table(schema = "public", name = "Battery_Data")
@Entity
@Data
public class BatteryEntity {


	@Id
	@GeneratedValue(strategy = SEQUENCE, generator = "batteryIdGen")
	@SequenceGenerator(name = "batteryIdGen2", schema = "public", sequenceName = "s_battery_seq", allocationSize = 1)
	private long id;

	@Column(name = "org_id")
	private Float orgId;

	@Column(name = "sender_id")
	private long deviceId;

	@Column(name = "sequene_number")
	private String sequeneNumber;

	@Column(name = "blive")
	private Integer bLive;
	@Column(name = "latitude")
	private Float latitude;
	@Column(name = "longitude")
	private Float longitude;
	@Column
	private String time;
	@Column
	private String date;

	@Column
	private String bin;

	@Column(name = "battery_status")
	private Integer batteryStatus;

	@Column
	private Integer soc;

	@Column
	private Integer soh;

	@Column
	private Integer sop;

	@Column(name = "equivalent_cycle")
	private float equivalentCycle;

	@Column(name = "minimum_cell_volatage")
	private float minimumCellVolatage;

	@Column(name = "maximum_cell_voltage")
	private float maximumCallVolatage;

	@Column(name = "minimum_cell_temperature")
	private float minimumCellTemperature;
	@Column(name = "maximum_cell_temperature")
	private float maximumCellTemperature;

	@Column
	private float current;

	@Column(name = "cycle_capacity")
	private float cycleCapacity;

	@Column(name = "build_number")
	private Integer bmsBuildNumber;

	@Column(name = "bms_configuration_version")
	private Integer bmsConfigurationVersion;

	@Column(name = "gps_data_valid_status")
	private String gpsValidDataStatus;

	@Column(name = "tcu_fw_version")
	private String tcuFwversion;

	@Column(name = "tcu_accx")
	private Integer tcuAccx;

	@Column(name = "tcu_accy")
	private Integer tcuAccy;

	@Column(name = "tcu_accz")
	private Integer tcuAccz;

	@Column(name = "warnings")
	private Integer warnings;

	@Column(name = "errors")
	private Integer errors;

	@Column(name = "internal_running_state")
	private Integer internalRunningState;

	@Column(name = "board_temperature")
	private float boardTemperature;

	@Column(name = "pdu_temperature")
	private float pduTemperature;

	@Column(name = "aging_debug_info_advance")
	private float agingDebugInfoAdvance;

	@Column(name = "aging_debug_info_impade")
	private float agingDebugInfoImpede;

	@Column(name = "events")
	private Integer events;

	@Column(name = "network_mode")
	private Integer networkMode;

	@Column(name = "network_information")
	private Integer networkInformation;

	@Column(name = "number_of_cells")
	private Integer numberOfCells;

	@Column(name = "number_of_temperature_sensors")
	private Integer numberOfTemperatureSensors;

	@Column(name = "cell_voltages")
	private String cellVoltages;
	
	@Column(name = "battery_voltage_cum")
	private String batteryVoltageCum;

	@Column(name = "cell_temperatures")
	private String cellTemperature;

    @Column
    private String tcuNewDate;

    @Column
    private String tcuNewTime;

    @Column
    private String gsmCsq;

    @Column
    private String tcuErrorInfo;

    @Column
    private String tamperedSwCount;

    @Column
    private Float noOfSatellites;

    @Column(name="strength_of_top5_satellites")
    private String strengthOfTop5Satellites;

    @Column
    private int numberOfBoardTemperature;

    @Column
    private float boardTemperatureAfe2;

    @Column
    private Long resetCounter;

    @Column
    private Integer tcuEvents;	

	@Column(name = "created_date")
	private Date createdDate;

	@Column
	private Date tcu_date;
	
	@Column
	private Date tcu_date_ist;


	@Column(name = "tcu_date_ist1")
	private Date tcu_date_ist1;

	@Column(name = "tcu_date1")
	private Date tcu_date1;

	@Column
	private Integer bms_reset_count;

	@Column
	private Integer bms_reset_reason;

	@Column
	private Integer platform;

	@Column
	private Long errors_1;

	@Column
	private Integer events_1;

	@Column
	private Integer network_information_1;

//	@Column
//	private Integer bms_triggered_tcu_reset_reason;

	@Column
	private Long bms_sequence_number;

	@Column
	private String sw_version;

	@Column
	private Integer mobilisation_status;

	@Column
	private Integer battery_type;

	@Column
	private String tcu_config_file_version;

}
