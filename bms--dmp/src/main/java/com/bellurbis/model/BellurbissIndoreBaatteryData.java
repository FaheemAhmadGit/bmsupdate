package com.bellurbis.model;

import static javax.persistence.GenerationType.SEQUENCE;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "Bellurbiss_Indore_battery_data")
public class BellurbissIndoreBaatteryData {
	@Id
	@GeneratedValue(strategy = SEQUENCE, generator = "batteryIdGen")
	@SequenceGenerator(name = "batteryIdGen2", schema = "public", sequenceName = "s_battery_seq", allocationSize = 1)
	 
	private int id;
    @Column(name = "sender_id")
     private long deviceId;
     private String topic;
     private String sequeneNumber;
     @Column(name = "blive")
     private Integer bLive;
     private String time;
     private float latitude;
     private float longitude;
     private String date;
     private String bin;
     private Integer batteryStatus;
     private Integer soc;
     private Integer soh;
     private Integer sop;
     private float equivalentCycle;
     private float minimumCellVolatage;
 	@Column(name = "maximum_cell_voltage")

     private float maximumCallVolatage;
     private float minimumCellTemperature;
     private float maximumCellTemperature;
     private float current;
     private float cycleCapacity;
 	@Column(name = "build_number")
     private String bmsBuildNumber;
     private float bmsConfigurationVersion;
     @Column(name = "gps_data_valid_status")
     private String gpsValidDataStatus;
 	@Column(name = "tcu_fw_version")
     private String tcuFwversion;
     private Integer tcuAccx;
     private Integer tcuAccy;
     private Integer tcuAccz;
     private Integer warnings;
     private Integer errors;
     private Integer internalRunningState;
 	@Column(name = "board_temperature")
 	private float boardTemperature;
     private float pduTemperature;
     private float agingDebugInfoAdvance;
 //	@Column(name = "aging_debug_info_impade")
     private float agingDebugInfoImpede;
     private Integer events;
     private float networkMode;
     private float networkInformation;
     private Integer numberOfCells;
     private Integer numberOfTemperatureSensors;
     private String cellVoltages;
     //private String cellVotagesStr ArrayList<String>;
     private float SumOfCellVoltages;
 	@Column(name = "cell_temperatures")
 	private String cellTemperature;

     private float SumOfCellTemperature;
     private Date tcuDate;
     private String tcuTime;
     private String gsmCsq;
     private String tcuErrorInfo;
     private String tamperedSwCount;
     private Float noOfSatellites;
    // private ArrayList<Float> strengthOfTop5Satellites;
     @Column(name="strength_of_top5_satellites")
     private String strengthOfTop5Satellites;

     private int numberOfBoardTemperature;
    
     private Integer resetCounter;
     private Integer tcuEvents;
     private Integer bms_reset_count;
     private Integer bms_reset_reason;
     private float platform;
     private Integer errors_1;
     private Integer events_1;
     private float network_information_1;
     private String bms_triggered_tcu_reset_reason;
     private long bms_sequence_number;
 	
     //extra
 	private String sw_version;
 	private String batteryVoltageCum;
 	private String battery_type;

 	@Column(name = "created_date")
 	private Date createdDate;
 	private String mobilisation_status;
 	@Column(name = "org_id")
 	private String orgId;
     @Column(name="board_temperature_afe2")
     private float boardTemperatureAfe2;

     @Column
     private String tcuNewDate;

     @Column
     private String tcuNewTime;
     @Column
 	private Date tcu_date_ist;


 	@Column(name = "tcu_date_ist1")
 	private Date tcu_date_ist1;

 	@Column(name = "tcu_date1")
 	private Date tcu_date1;


}
