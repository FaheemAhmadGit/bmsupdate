package com.bellurbis.model;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bellurbis.common.CommonUtil;

import java.util.ArrayList;
import java.util.Date;

import static java.lang.Float.parseFloat;
import static java.lang.Integer.parseInt;

import java.sql.Timestamp;
import java.text.ParseException;

@Slf4j
public class PayloadParser {
	
	@Autowired
	private static CommonUtil commonUtil;

	public static BellubissGurugramBatteryData stringParserMapper(String message, String topic) throws ParseException {
		ArrayList<String> cellVoltages = new ArrayList<>();
		ArrayList<String> cellTemperatures = new ArrayList<>();
		ArrayList<Float> strengthOfSatellites = new ArrayList<>();
		String[] s = message.split(",");
		Date date = new Date();
		BellubissGurugramBatteryData mParam = new BellubissGurugramBatteryData();
		mParam.setTopic(topic);
		mParam.setDeviceId(Long.parseLong(s[1]));
		mParam.setSequeneNumber(s[2]);
		mParam.setBLive(parseInt(s[3]));
		mParam.setGpsValidDataStatus(s[4]);
		mParam.setLatitude(Float.parseFloat(s[5]));
		mParam.setLongitude(Float.parseFloat(s[6]));
		mParam.setTime(s[7]);
		mParam.setDate(s[8]);

		mParam.setTcuFwversion(s[9]);
		mParam.setTcuAccx(parseInt(s[10]));
		mParam.setTcuAccy(parseInt(s[11]));
		mParam.setTcuAccz(parseInt(s[12]));
		mParam.setBin(s[13]);
		if (mParam.getBin().startsWith("00000000000")) {
			log.error(message);
		}
		mParam.setBatteryStatus(Integer.parseInt(s[14]));
		mParam.setSoc(Integer.parseInt(s[15]));
		mParam.setSoh(Integer.parseInt(s[16]));
		mParam.setEquivalentCycle(parseFloat(s[17]));
		mParam.setBmsBuildNumber((s[18]));
		mParam.setCycleCapacity(parseFloat(s[19]));
		mParam.setSop(Integer.parseInt(s[20]));
		mParam.setWarnings(parseInt(s[21]));
		mParam.setErrors(parseInt(s[22]));
		mParam.setCurrent(parseFloat(s[23]));
		mParam.setInternalRunningState(parseInt(s[24]));
		// mParam.setBoardTemperatureAfe1(Float.parseFloat(s[25]));
		mParam.setPduTemperature(Float.parseFloat(s[26]));
		mParam.setAgingDebugInfoAdvance(Float.parseFloat(s[27]));
		mParam.setAgingDebugInfoImpede(Float.parseFloat(s[28]));
		mParam.setMinimumCellVolatage(parseFloat(s[29]));
		mParam.setMaximumCallVolatage(parseFloat(s[30]));
		mParam.setMinimumCellTemperature(parseFloat(s[31]));
		mParam.setMaximumCellTemperature(parseFloat(s[32]));
		mParam.setEvents(Integer.parseInt(s[33]));
		mParam.setNetworkMode(Integer.parseInt(s[34]));
		mParam.setNetworkInformation(Integer.parseInt(s[35]));
		mParam.setNumberOfCells(Integer.parseInt(s[36]));
		mParam.setSw_version(s[37]);
		mParam.setBatteryVoltageCum(s[38]);
		mParam.setBattery_type((s[39]));
		mParam.setCreatedDate(new Timestamp(date.getTime()));
		mParam.setOrgId((s[41]));
		mParam.setMobilisation_status((s[42]));
		mParam.setBoardTemperatureAfe2(Float.parseFloat(s[43]));
		mParam.setTcu_date_ist(new Timestamp(date.getTime()));
		mParam.setTcu_date1(new Timestamp(date.getTime()));
		
//		mParam.setTcu_date_ist(commonUtil.convertSimpleDateFormat(commonUtil.getModifiedNewTimeinISTForRBML(mParam)));
//              try {
//                  mParam.setTcu_date_ist(commonUtil.convertSimpleDateFormat(commonUtil.getModifiedTimeinIST(b)));
//                  b.setTcu_date(commonUtil.getModifiedTimeinIST(mParam));
//                  b.setTcu_date_ist1(commonUtil.convertSimpleDateFormat(commonUtil.getModifiedNewTimeinIST(b)));
//                  b.setTcu_date1(commonUtil.getModifiedNewTimeinIST(b));
//              } catch (ParseException e) {
//                  e.printStackTrace();
//              }
		cellVoltageAndTemperatureMapper(cellVoltages, cellTemperatures, s, mParam);

		if (s.length > 75) {

			String var = s[s.length - 1].replaceAll("[|#]", "");
			mParam.setBms_sequence_number(Long.parseLong(var));
			mParam.setBms_triggered_tcu_reset_reason((s[s.length - 2]));
			mParam.setNetwork_information_1(Float.parseFloat(s[s.length - 3]));
			mParam.setEvents_1(Integer.parseInt(s[s.length - 4]));
			mParam.setErrors_1(Integer.parseInt(s[s.length - 5]));
			mParam.setPlatform(Float.parseFloat(s[s.length - 6]));
			mParam.setBms_reset_reason(Integer.parseInt(s[s.length - 7]));
			mParam.setBms_reset_count(Integer.parseInt(s[s.length - 8]));
			////////////

			// String var = s[s.length - 1].replaceAll("[|#]", "");
			if (mParam.getCellTemperature().equals(s[s.length - 10])) {
				mParam.setBmsConfigurationVersion(Integer.parseInt(s[s.length - 9]));
			} else {
				mParam.setTcuEvents(Integer.parseInt(s[s.length - 9]));
				mParam.setResetCounter(Integer.parseInt(s[s.length - 10]));
				// mParam.setBoardTemperatureAfe2(Float.parseFloat(s[s.length - 11]));
				mParam.setNumberOfBoardTemperature(Integer.parseInt(s[s.length - 12]));
				for (int i = 9; i <= 13; i++) {
					strengthOfSatellites.add(Float.parseFloat(s[s.length - 12 - i]));
				}
				mParam.setStrengthOfTop5Satellites(strengthOfSatellites.toString());
				mParam.setNoOfSatellites(Float.parseFloat(s[s.length - 18]));
				mParam.setTamperedSwCount(s[s.length - 19]);
				mParam.setTcuErrorInfo(s[s.length - 20]);
				mParam.setGsmCsq(s[s.length - 21]);
				mParam.setTcuDate(new Timestamp(date.getTime()));
				mParam.setTcuTime(s[s.length - 23]);
				mParam.setBmsConfigurationVersion(Float.parseFloat(s[s.length - 24]));
			}

		}

		else {

			String var = s[s.length - 1].replaceAll("[|#]", "");
			if (mParam.getCellTemperature().equals(s[s.length - 2])) {
				mParam.setBmsConfigurationVersion(Integer.parseInt(var));
			} else {
				mParam.setTcuEvents(Integer.parseInt(var));
				mParam.setResetCounter(Integer.parseInt(s[s.length - 2]));
				// mParam.setBoardTemperatureAfe2(Float.parseFloat(s[s.length - 3]));
				mParam.setNumberOfBoardTemperature(Integer.parseInt(s[s.length - 4]));
				for (int i = 1; i <= 5; i++) {
					strengthOfSatellites.add(Float.parseFloat(s[s.length - 4 - i]));
				}
				mParam.setStrengthOfTop5Satellites(strengthOfSatellites.toString());
				mParam.setNoOfSatellites(Float.parseFloat(s[s.length - 10]));
				mParam.setTamperedSwCount(s[s.length - 11]);
				mParam.setTcuErrorInfo(s[s.length - 12]);
				mParam.setGsmCsq(s[s.length - 13]);
				mParam.setTcuDate(new Timestamp(date.getTime()));
				mParam.setTcuTime(s[s.length - 15]);
				mParam.setBmsConfigurationVersion(Integer.parseInt(s[s.length - 16]));
			}

		}

		log.info(String.valueOf(mParam));
		return mParam;
	}
	private static void cellVoltageAndTemperatureMapper(ArrayList<String> cellVoltages,
			ArrayList<String> cellTemperatures, String[] s, BellubissGurugramBatteryData mParam) {
		Integer maxIndexOfCellVoltages = 36 + 16;
		int arrayIndexOfCellVoltages = 0;
		float sumOfVoltage = 0;
		float sumOfTemperature = 0;
		int i;
		for (i = 37; i <= maxIndexOfCellVoltages; i++) {
			cellVoltages.add(s[i]);
			arrayIndexOfCellVoltages = i;
			sumOfVoltage = sumOfVoltage + Float.parseFloat(s[i]);
		}
		// mParam.setSumOfCellVoltages(sumOfVoltage);
		mParam.setNumberOfTemperatureSensors(Integer.parseInt(s[arrayIndexOfCellVoltages + 1]));
		mParam.setCellVoltages(cellVoltages.toString());
		int maxOfCellTemperaturesCount = arrayIndexOfCellVoltages + 1 + 5;
		i = arrayIndexOfCellVoltages + 2;
		for (; i <= maxOfCellTemperaturesCount; i++) {
			cellTemperatures.add(s[i]);
			sumOfTemperature += Float.parseFloat(s[i]);
		}
		// mParam.setSumOfCellTemperature(sumOfTemperature);
		mParam.setCellTemperature(cellTemperatures.toString());
	}

	public static BellurbissIndoreBaatteryData stringParserMapperForRRL(String message, String topic) {
		ArrayList<String> cellVoltages = new ArrayList<>();
		ArrayList<String> cellTemperatures = new ArrayList<>();
		ArrayList<Float> strengthOfSatellites = new ArrayList<>();
		String[] s = message.split(",");
		Date date = new Date();
		BellurbissIndoreBaatteryData mParam = new BellurbissIndoreBaatteryData();
		mParam.setTopic(topic);
		mParam.setDeviceId(Long.parseLong(s[1]));
		mParam.setSequeneNumber(s[2]);
		mParam.setBLive(parseInt(s[3]));
		mParam.setGpsValidDataStatus(s[4]);
		mParam.setLatitude(Float.parseFloat(s[5]));
		mParam.setLongitude(Float.parseFloat(s[6]));
		mParam.setTime(s[7]);
		mParam.setDate(s[8]);

		mParam.setTcuFwversion(s[9]);
		mParam.setTcuAccx(parseInt(s[10]));
		mParam.setTcuAccy(parseInt(s[11]));
		mParam.setTcuAccz(parseInt(s[12]));
		mParam.setBin(s[13]);
		if (mParam.getBin().startsWith("00000000000")) {
			log.error(message);
		}
		mParam.setBatteryStatus(Integer.parseInt(s[14]));
		mParam.setSoc(Integer.parseInt(s[15]));
		mParam.setSoh(Integer.parseInt(s[16]));
		mParam.setEquivalentCycle(parseFloat(s[17]));
		mParam.setBmsBuildNumber((s[18]));
		mParam.setCycleCapacity(parseFloat(s[19]));
		mParam.setSop(Integer.parseInt(s[20]));
		mParam.setWarnings(parseInt(s[21]));
		mParam.setErrors(parseInt(s[22]));
		mParam.setCurrent(parseFloat(s[23]));
		mParam.setInternalRunningState(parseInt(s[24]));
		// mParam.setBoardTemperatureAfe1(Float.parseFloat(s[25]));
		mParam.setPduTemperature(Float.parseFloat(s[26]));
		mParam.setAgingDebugInfoAdvance(Float.parseFloat(s[27]));
		mParam.setAgingDebugInfoImpede(Float.parseFloat(s[28]));
		mParam.setMinimumCellVolatage(parseFloat(s[29]));
		mParam.setMaximumCallVolatage(parseFloat(s[30]));
		mParam.setMinimumCellTemperature(parseFloat(s[31]));
		mParam.setMaximumCellTemperature(parseFloat(s[32]));
		mParam.setEvents(Integer.parseInt(s[33]));
		mParam.setNetworkMode(Integer.parseInt(s[34]));
		mParam.setNetworkInformation(Integer.parseInt(s[35]));
		mParam.setNumberOfCells(Integer.parseInt(s[36]));
		mParam.setSw_version(s[37]);
		mParam.setBatteryVoltageCum(s[38]);
		mParam.setBattery_type((s[39]));
		mParam.setCreatedDate(new Timestamp(date.getTime()));
		mParam.setOrgId((s[41]));
		mParam.setMobilisation_status((s[42]));
		mParam.setBoardTemperatureAfe2(Float.parseFloat(s[43]));
		mParam.setTcu_date_ist(new Timestamp(date.getTime()));
		mParam.setTcu_date1(new Timestamp(date.getTime()));
		mParam.setTcu_date_ist1(new Timestamp(date.getTime()));
//              try {
//                  mParam.setTcu_date_ist(commonUtil.convertSimpleDateFormat(commonUtil.getModifiedTimeinIST(b)));
//                  b.setTcu_date(commonUtil.getModifiedTimeinIST(mParam));
//                  b.setTcu_date_ist1(commonUtil.convertSimpleDateFormat(commonUtil.getModifiedNewTimeinIST(b)));
//                  b.setTcu_date1(commonUtil.getModifiedNewTimeinIST(b));
//              } catch (ParseException e) {
//                  e.printStackTrace();
//              }
		cellVoltageAndTemperatureMapper(cellVoltages, cellTemperatures, s, mParam);

		if (s.length > 75) {

			String var = s[s.length - 1].replaceAll("[|#]", "");
			mParam.setBms_sequence_number(Long.parseLong(var));
			mParam.setBms_triggered_tcu_reset_reason((s[s.length - 2]));
			mParam.setNetwork_information_1(Float.parseFloat(s[s.length - 3]));
			mParam.setEvents_1(Integer.parseInt(s[s.length - 4]));
			mParam.setErrors_1(Integer.parseInt(s[s.length - 5]));
			mParam.setPlatform(Float.parseFloat(s[s.length - 6]));
			mParam.setBms_reset_reason(Integer.parseInt(s[s.length - 7]));
			mParam.setBms_reset_count(Integer.parseInt(s[s.length - 8]));
			////////////

			// String var = s[s.length - 1].replaceAll("[|#]", "");
			if (mParam.getCellTemperature().equals(s[s.length - 10])) {
				mParam.setBmsConfigurationVersion(Integer.parseInt(s[s.length - 9]));
			} else {
				mParam.setTcuEvents(Integer.parseInt(s[s.length - 9]));
				mParam.setResetCounter(Integer.parseInt(s[s.length - 10]));
				// mParam.setBoardTemperatureAfe2(Float.parseFloat(s[s.length - 11]));
				mParam.setNumberOfBoardTemperature(Integer.parseInt(s[s.length - 12]));
				for (int i = 9; i <= 13; i++) {
					strengthOfSatellites.add(Float.parseFloat(s[s.length - 12 - i]));
				}
				mParam.setStrengthOfTop5Satellites(strengthOfSatellites.toString());
				mParam.setNoOfSatellites(Float.parseFloat(s[s.length - 18]));
				mParam.setTamperedSwCount(s[s.length - 19]);
				mParam.setTcuErrorInfo(s[s.length - 20]);
				mParam.setGsmCsq(s[s.length - 21]);
				mParam.setTcuDate(new Timestamp(date.getTime()));
				mParam.setTcuTime(s[s.length - 23]);
				mParam.setBmsConfigurationVersion(Float.parseFloat(s[s.length - 24]));
			}

		}

		else {

			String var = s[s.length - 1].replaceAll("[|#]", "");
			if (mParam.getCellTemperature().equals(s[s.length - 2])) {
				mParam.setBmsConfigurationVersion(Integer.parseInt(var));
			} else {
				mParam.setTcuEvents(Integer.parseInt(var));
				mParam.setResetCounter(Integer.parseInt(s[s.length - 2]));
				// mParam.setBoardTemperatureAfe2(Float.parseFloat(s[s.length - 3]));
				mParam.setNumberOfBoardTemperature(Integer.parseInt(s[s.length - 4]));
				for (int i = 1; i <= 5; i++) {
					strengthOfSatellites.add(Float.parseFloat(s[s.length - 4 - i]));
				}
				mParam.setStrengthOfTop5Satellites(strengthOfSatellites.toString());
				mParam.setNoOfSatellites(Float.parseFloat(s[s.length - 10]));
				mParam.setTamperedSwCount(s[s.length - 11]);
				mParam.setTcuErrorInfo(s[s.length - 12]);
				mParam.setGsmCsq(s[s.length - 13]);
				mParam.setTcuDate(new Timestamp(date.getTime()));
				mParam.setTcuTime(s[s.length - 15]);
				mParam.setBmsConfigurationVersion(Integer.parseInt(s[s.length - 16]));
			}

		}

		log.info(String.valueOf(mParam));
		return mParam;

	}

	private static void cellVoltageAndTemperatureMapper(ArrayList<String> cellVoltages,
			ArrayList<String> cellTemperatures, String[] s, BellurbissIndoreBaatteryData mParam) {
		Integer maxIndexOfCellVoltages = 36 + 16;
		int arrayIndexOfCellVoltages = 0;
		float sumOfVoltage = 0;
		float sumOfTemperature = 0;
		int i;
		for (i = 37; i <= maxIndexOfCellVoltages; i++) {
			cellVoltages.add(s[i]);
			arrayIndexOfCellVoltages = i;
			sumOfVoltage = sumOfVoltage + Float.parseFloat(s[i]);
		}
		// mParam.setSumOfCellVoltages(sumOfVoltage);
		mParam.setNumberOfTemperatureSensors(Integer.parseInt(s[arrayIndexOfCellVoltages + 1]));
		mParam.setCellVoltages(cellVoltages.toString());
		int maxOfCellTemperaturesCount = arrayIndexOfCellVoltages + 1 + 5;
		i = arrayIndexOfCellVoltages + 2;
		for (; i <= maxOfCellTemperaturesCount; i++) {
			cellTemperatures.add(s[i]);
			sumOfTemperature += Float.parseFloat(s[i]);
		}
		// mParam.setSumOfCellTemperature(sumOfTemperature);
		mParam.setCellTemperature(cellTemperatures.toString());
	}

	public static BatteryEntity stringParserMapperForBatteryEntity(String message, String topic) {
		ArrayList<String> cellVoltages = new ArrayList<>();
		ArrayList<String> cellTemperatures = new ArrayList<>();
		ArrayList<Float> strengthOfSatellites = new ArrayList<>();
		String[] s = message.split(",");
		Date date = new Date();
		BatteryEntity mParam = new BatteryEntity();
		// mParam.setTopic(topic);
		mParam.setDeviceId(Long.parseLong(s[1]));
		mParam.setSequeneNumber(s[2]);
		mParam.setBLive(parseInt(s[3]));
		mParam.setGpsValidDataStatus(s[4]);
		mParam.setLatitude(Float.parseFloat(s[5]));
		 mParam.setLongitude(Float.parseFloat(s[6]));
		mParam.setTime(s[7]);
		mParam.setDate(s[8]);

		mParam.setTcuFwversion(s[9]);
		mParam.setTcuAccx(parseInt(s[10]));
		mParam.setTcuAccy(parseInt(s[11]));
		mParam.setTcuAccz(parseInt(s[12]));
		mParam.setBin(s[13]);
		if (mParam.getBin().startsWith("00000000000")) {
			log.error(message);
		}
		mParam.setBatteryStatus(Integer.parseInt(s[14]));
		mParam.setSoc(Integer.parseInt(s[15]));
		mParam.setSoh(Integer.parseInt(s[16]));
		mParam.setEquivalentCycle(parseFloat(s[17]));
		// mParam.setBmsBuildNumber((s[18]));
		mParam.setCycleCapacity(parseFloat(s[19]));
		mParam.setSop(Integer.parseInt(s[20]));
		mParam.setWarnings(parseInt(s[21]));
		mParam.setErrors(parseInt(s[22]));
		mParam.setCurrent(parseFloat(s[23]));
		mParam.setInternalRunningState(parseInt(s[24]));
		// mParam.setBoardTemperatureAfe1(Float.parseFloat(s[25]));
		mParam.setPduTemperature(Float.parseFloat(s[26]));
		mParam.setAgingDebugInfoAdvance(Float.parseFloat(s[27]));
		mParam.setAgingDebugInfoImpede(Float.parseFloat(s[28]));
		mParam.setMinimumCellVolatage(parseFloat(s[29]));
		mParam.setMaximumCallVolatage(parseFloat(s[30]));
		mParam.setMinimumCellTemperature(parseFloat(s[31]));
		mParam.setMaximumCellTemperature(parseFloat(s[32]));
		mParam.setEvents(Integer.parseInt(s[33]));
		mParam.setNetworkMode(Integer.parseInt(s[34]));
		mParam.setNetworkInformation(Integer.parseInt(s[35]));
		mParam.setNumberOfCells(Integer.parseInt(s[36]));
		mParam.setSw_version(s[37]);
		mParam.setBatteryVoltageCum(s[38]);
		// mParam.setBattery_type((s[39]));
		mParam.setCreatedDate(new Timestamp(date.getTime()));
		mParam.setOrgId(Float.parseFloat(s[41]));
		// mParam.setMobilisation_status((s[42]));
		mParam.setBoardTemperatureAfe2(Float.parseFloat(s[43]));
		mParam.setTcu_date_ist(new Timestamp(date.getTime()));
		mParam.setTcu_date1(new Timestamp(date.getTime()));
		mParam.setTcu_date_ist1(new Timestamp(date.getTime()));
//            try {
//                mParam.setTcu_date_ist(commonUtil.convertSimpleDateFormat(commonUtil.getModifiedTimeinIST(b)));
//                b.setTcu_date(commonUtil.getModifiedTimeinIST(mParam));
//                b.setTcu_date_ist1(commonUtil.convertSimpleDateFormat(commonUtil.getModifiedNewTimeinIST(b)));
//                b.setTcu_date1(commonUtil.getModifiedNewTimeinIST(b));
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }
		cellVoltageAndTemperatureMapper(cellVoltages, cellTemperatures, s, mParam);

		if (s.length > 75) {

			String var = s[s.length - 1].replaceAll("[|#]", "");
			mParam.setBms_sequence_number(Long.parseLong(var));
//                mParam.setBms_triggered_tcu_reset_reason((s[s.length - 2]));
//                mParam.setNetwork_information_1(Float.parseFloat(s[s.length - 3]));
//                mParam.setEvents_1(Integer.parseInt(s[s.length - 4]));
//                mParam.setErrors_1(Integer.parseInt(s[s.length - 5]));
//                mParam.setPlatform(Float.parseFloat(s[s.length - 6]));
			mParam.setBms_reset_reason(Integer.parseInt(s[s.length - 7]));
			mParam.setBms_reset_count(Integer.parseInt(s[s.length - 8]));
			////////////

			// String var = s[s.length - 1].replaceAll("[|#]", "");
			if (mParam.getCellTemperature().equals(s[s.length - 10])) {
				mParam.setBmsConfigurationVersion(Integer.parseInt(s[s.length - 9]));
			} else {
				mParam.setTcuEvents(Integer.parseInt(s[s.length - 9]));
				// mParam.setResetCounter(Integer.parseInt(s[s.length - 10]));
				// mParam.setBoardTemperatureAfe2(Float.parseFloat(s[s.length - 11]));
				mParam.setNumberOfBoardTemperature(Integer.parseInt(s[s.length - 12]));
				for (int i = 9; i <= 13; i++) {
					strengthOfSatellites.add(Float.parseFloat(s[s.length - 12 - i]));
				}
				mParam.setStrengthOfTop5Satellites(strengthOfSatellites.toString());
				mParam.setNoOfSatellites(Float.parseFloat(s[s.length - 18]));
				mParam.setTamperedSwCount(s[s.length - 19]);
				mParam.setTcuErrorInfo(s[s.length - 20]);
				mParam.setGsmCsq(s[s.length - 21]);
//                    mParam.setTcuDate(s[s.length - 22]);
//                    mParam.setTcuTime(s[s.length - 23]);
				mParam.setBmsConfigurationVersion(Integer.parseInt(s[s.length - 24]));
			}

		}

		else {

			String var = s[s.length - 1].replaceAll("[|#]", "");
			if (mParam.getCellTemperature().equals(s[s.length - 2])) {
				mParam.setBmsConfigurationVersion(Integer.parseInt(var));
			} else {
		//		mParam.setTcuEvents(Integer.parseInt(var));
				// mParam.setResetCounter(Integer.parseInt(s[s.length - 2]));
				// mParam.setBoardTemperatureAfe2(Float.parseFloat(s[s.length - 3]));
				mParam.setNumberOfBoardTemperature(Integer.parseInt(s[s.length - 4]));
				for (int i = 1; i <= 5; i++) {
					strengthOfSatellites.add(Float.parseFloat(s[s.length - 4 - i]));
				}
				mParam.setStrengthOfTop5Satellites(strengthOfSatellites.toString());
				mParam.setNoOfSatellites(Float.parseFloat(s[s.length - 10]));
				mParam.setTamperedSwCount(s[s.length - 11]);
				mParam.setTcuErrorInfo(s[s.length - 12]);
				mParam.setGsmCsq(s[s.length - 13]);
//                    mParam.setTcuDate(s[s.length - 14]);
//                    mParam.setTcuTime(s[s.length - 15]);
				mParam.setBmsConfigurationVersion(Integer.parseInt(s[s.length - 16]));
			}

		}

		log.info(String.valueOf(mParam));
		return mParam;

	}

	private static void cellVoltageAndTemperatureMapper(ArrayList<String> cellVoltages,
			ArrayList<String> cellTemperatures, String[] s, BellurbissBatteryData mParam) {
		Integer maxIndexOfCellVoltages = 36 + 16;
		int arrayIndexOfCellVoltages = 0;
		float sumOfVoltage = 0;
		float sumOfTemperature = 0;
		int i;
		for (i = 37; i <= maxIndexOfCellVoltages; i++) {
			cellVoltages.add(s[i]);
			arrayIndexOfCellVoltages = i;
			sumOfVoltage = sumOfVoltage + Float.parseFloat(s[i]);
		}
		// mParam.setSumOfCellVoltages(sumOfVoltage);
		mParam.setNumberOfTemperatureSensors(Integer.parseInt(s[arrayIndexOfCellVoltages + 1]));
		mParam.setCellVoltages(cellVoltages.toString());
		int maxOfCellTemperaturesCount = arrayIndexOfCellVoltages + 1 + 5; ////// >>>>>>>>>>>>>>> HARD Code Value for 5
																			////// Cell Temperature
		i = arrayIndexOfCellVoltages + 2;
		for (; i <= maxOfCellTemperaturesCount; i++) {
			cellTemperatures.add(s[i]);
			sumOfTemperature += Float.parseFloat(s[i]);
		}
		// mParam.setSumOfCellTemperature(sumOfTemperature);
		mParam.setCellTemperature(cellTemperatures.toString());
	}

	public static BellurbissBatteryData stringParserMapperForExicom(String message, String topic) {
		ArrayList<String> cellVoltages = new ArrayList<>();
		ArrayList<String> cellTemperatures = new ArrayList<>();
		ArrayList<Float> strengthOfSatellites = new ArrayList<>();
		String[] s = message.split(",");
		Date date = new Date();
		BellurbissBatteryData mParam = new BellurbissBatteryData();
		mParam.setTopic(topic);
		mParam.setDeviceId(Long.parseLong(s[1]));
		mParam.setSequeneNumber(s[2]);
		mParam.setBLive(parseInt(s[3]));
		mParam.setGpsValidDataStatus(s[4]);
		mParam.setLatitude(Float.parseFloat(s[5]));
		mParam.setLongitude(Float.parseFloat(s[6]));
		mParam.setTime(s[7]);
		mParam.setDate(s[8]);

		mParam.setTcuFwversion(s[9]);
		mParam.setTcuAccx(parseInt(s[10]));
		mParam.setTcuAccy(parseInt(s[11]));
		mParam.setTcuAccz(parseInt(s[12]));
		mParam.setBin(s[13]);
		if (mParam.getBin().startsWith("00000000000")) {
			log.error(message);
		}
		mParam.setBatteryStatus(Integer.parseInt(s[14]));
		mParam.setSoc(Integer.parseInt(s[15]));
		mParam.setSoh(Integer.parseInt(s[16]));
		mParam.setEquivalentCycle(parseFloat(s[17]));
		mParam.setBmsBuildNumber((s[18]));
		mParam.setCycleCapacity(parseFloat(s[19]));
		mParam.setSop(Integer.parseInt(s[20]));
		mParam.setWarnings(parseInt(s[21]));
		mParam.setErrors(parseInt(s[22]));
		mParam.setCurrent(parseFloat(s[23]));
		mParam.setInternalRunningState(parseInt(s[24]));
		// mParam.setBoardTemperatureAfe1(Float.parseFloat(s[25]));
		mParam.setPduTemperature(Float.parseFloat(s[26]));
		mParam.setAgingDebugInfoAdvance(Float.parseFloat(s[27]));
		mParam.setAgingDebugInfoImpede(Float.parseFloat(s[28]));
		mParam.setMinimumCellVolatage(parseFloat(s[29]));
		mParam.setMaximumCallVolatage(parseFloat(s[30]));
		mParam.setMinimumCellTemperature(parseFloat(s[31]));
		mParam.setMaximumCellTemperature(parseFloat(s[32]));
		mParam.setEvents(Integer.parseInt(s[33]));
		mParam.setNetworkMode(Integer.parseInt(s[34]));
		mParam.setNetworkInformation(Integer.parseInt(s[35]));
		mParam.setNumberOfCells(Integer.parseInt(s[36]));
		mParam.setSw_version(s[37]);
		mParam.setBatteryVoltageCum(s[38]);
		mParam.setBattery_type((s[39]));
		mParam.setCreatedDate(new Timestamp(date.getTime()));
		// mParam.setOrgId((s[41]));
		mParam.setMobilisation_status((s[42]));
		mParam.setBoardTemperatureAfe2(Float.parseFloat(s[43]));
		mParam.setTcu_date_ist(new Timestamp(date.getTime()));
		mParam.setTcu_date1(new Timestamp(date.getTime()));
		mParam.setTcu_date_ist1(new Timestamp(date.getTime()));
//            try {
//                mParam.setTcu_date_ist(commonUtil.convertSimpleDateFormat(commonUtil.getModifiedTimeinIST(b)));
//                b.setTcu_date(commonUtil.getModifiedTimeinIST(mParam));
//                b.setTcu_date_ist1(commonUtil.convertSimpleDateFormat(commonUtil.getModifiedNewTimeinIST(b)));
//                b.setTcu_date1(commonUtil.getModifiedNewTimeinIST(b));
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }
		cellVoltageAndTemperatureMapper(cellVoltages, cellTemperatures, s, mParam);

		if (s.length > 75) {

			String var = s[s.length - 1].replaceAll("[|#]", "");
			mParam.setBms_sequence_number(Long.parseLong(var));
			mParam.setBms_triggered_tcu_reset_reason((s[s.length - 2]));
			mParam.setNetwork_information_1(Float.parseFloat(s[s.length - 3]));
			mParam.setEvents_1(Integer.parseInt(s[s.length - 4]));
			mParam.setErrors_1(Integer.parseInt(s[s.length - 5]));
			mParam.setPlatform(Float.parseFloat(s[s.length - 6]));
			mParam.setBms_reset_reason(Integer.parseInt(s[s.length - 7]));
			mParam.setBms_reset_count(Integer.parseInt(s[s.length - 8]));
			////////////

			// String var = s[s.length - 1].replaceAll("[|#]", "");
			if (mParam.getCellTemperature().equals(s[s.length - 10])) {
				mParam.setBmsConfigurationVersion(Integer.parseInt(s[s.length - 9]));
			} else {
				mParam.setTcuEvents(Integer.parseInt(s[s.length - 9]));
				mParam.setResetCounter(Integer.parseInt(s[s.length - 10]));
				// mParam.setBoardTemperatureAfe2(Float.parseFloat(s[s.length - 11]));
				mParam.setNumberOfBoardTemperature(Integer.parseInt(s[s.length - 12]));
				for (int i = 9; i <= 13; i++) {
					strengthOfSatellites.add(Float.parseFloat(s[s.length - 12 - i]));
				}
				mParam.setStrengthOfTop5Satellites(strengthOfSatellites.toString());
				mParam.setNoOfSatellites(Float.parseFloat(s[s.length - 18]));
				mParam.setTamperedSwCount(s[s.length - 19]);
				mParam.setTcuErrorInfo(s[s.length - 20]);
				mParam.setGsmCsq(s[s.length - 21]);
				mParam.setTcuDate(new Timestamp(date.getTime()));
				mParam.setTcuTime(s[s.length - 23]);
				mParam.setBmsConfigurationVersion(Float.parseFloat(s[s.length - 24]));
			}

		}

		else {

			String var = s[s.length - 1].replaceAll("[|#]", "");
			if (mParam.getCellTemperature().equals(s[s.length - 2])) {
				mParam.setBmsConfigurationVersion(Integer.parseInt(var));
			} else {
				mParam.setTcuEvents(Integer.parseInt(var));
				mParam.setResetCounter(Integer.parseInt(s[s.length - 2]));
				// mParam.setBoardTemperatureAfe2(Float.parseFloat(s[s.length - 3]));
				mParam.setNumberOfBoardTemperature(Integer.parseInt(s[s.length - 4]));
				for (int i = 1; i <= 5; i++) {
					strengthOfSatellites.add(Float.parseFloat(s[s.length - 4 - i]));
				}
				mParam.setStrengthOfTop5Satellites(strengthOfSatellites.toString());
				mParam.setNoOfSatellites(Float.parseFloat(s[s.length - 10]));
				mParam.setTamperedSwCount(s[s.length - 11]);
				mParam.setTcuErrorInfo(s[s.length - 12]);
				mParam.setGsmCsq(s[s.length - 13]);
				mParam.setTcuDate(new Timestamp(date.getTime()));
				mParam.setTcuTime(s[s.length - 15]);
				mParam.setBmsConfigurationVersion(Integer.parseInt(s[s.length - 16]));
			}

		}

		log.info(String.valueOf(mParam));
		return mParam;

	}

	private static void cellVoltageAndTemperatureMapper(ArrayList<String> cellVoltages,
			ArrayList<String> cellTemperatures, String[] s, BatteryEntity mParam) {
		Integer maxIndexOfCellVoltages = 36 + 16;
		int arrayIndexOfCellVoltages = 0;
		float sumOfVoltage = 0;
		float sumOfTemperature = 0;
		int i;
		for (i = 37; i <= maxIndexOfCellVoltages; i++) {
			cellVoltages.add(s[i]);
			arrayIndexOfCellVoltages = i;
			sumOfVoltage = sumOfVoltage + Float.parseFloat(s[i]);
		}
		// mParam.setSumOfCellVoltages(sumOfVoltage);
		mParam.setNumberOfTemperatureSensors(Integer.parseInt(s[arrayIndexOfCellVoltages + 1]));
		mParam.setCellVoltages(cellVoltages.toString());
		int maxOfCellTemperaturesCount = arrayIndexOfCellVoltages + 1 + 5; ////// >>>>>>>>>>>>>>> HARD Code Value for 5
																			////// Cell Temperature
		i = arrayIndexOfCellVoltages + 2;
		for (; i <= maxOfCellTemperaturesCount; i++) {
			cellTemperatures.add(s[i]);
			sumOfTemperature += Float.parseFloat(s[i]);
		}
		// mParam.setSumOfCellTemperature(sumOfTemperature);
		mParam.setCellTemperature(cellTemperatures.toString());
	}

	public static BellurbisUSABatteryData stringParserMapperForExicomBounce(String message, String topic) {
		ArrayList<String> cellVoltages = new ArrayList<>();
		ArrayList<String> cellTemperatures = new ArrayList<>();
		ArrayList<Float> strengthOfSatellites = new ArrayList<>();
		String[] s = message.split(",");
		Date date = new Date();
		BellurbisUSABatteryData mParam = new BellurbisUSABatteryData();
		mParam.setTopic(topic);
		mParam.setDeviceId(Long.parseLong(s[1]));
		mParam.setSequeneNumber(s[2]);
		mParam.setBLive(parseInt(s[3]));
		mParam.setGpsValidDataStatus(s[4]);
		mParam.setLatitude(Float.parseFloat(s[5]));
		mParam.setLongitude(Float.parseFloat(s[6]));
		mParam.setTime(s[7]);
		mParam.setDate(s[8]);

		mParam.setTcuFwversion(s[9]);
		mParam.setTcuAccx(parseInt(s[10]));
		mParam.setTcuAccy(parseInt(s[11]));
		mParam.setTcuAccz(parseInt(s[12]));
		mParam.setBin(s[13]);
		if (mParam.getBin().startsWith("00000000000")) {
			log.error(message);
		}
		mParam.setBatteryStatus(Integer.parseInt(s[14]));
		mParam.setSoc(Integer.parseInt(s[15]));
		mParam.setSoh(Integer.parseInt(s[16]));
		mParam.setEquivalentCycle(parseFloat(s[17]));
		mParam.setBmsBuildNumber((s[18]));
		mParam.setCycleCapacity(parseFloat(s[19]));
		mParam.setSop(Integer.parseInt(s[20]));
		mParam.setWarnings(parseInt(s[21]));
		mParam.setErrors(parseInt(s[22]));
		mParam.setCurrent(parseFloat(s[23]));
		mParam.setInternalRunningState(parseInt(s[24]));
		// mParam.setBoardTemperatureAfe1(Float.parseFloat(s[25]));
		mParam.setPduTemperature(Float.parseFloat(s[26]));
		mParam.setAgingDebugInfoAdvance(Float.parseFloat(s[27]));
		mParam.setAgingDebugInfoImpede(Float.parseFloat(s[28]));
		mParam.setMinimumCellVolatage(parseFloat(s[29]));
		mParam.setMaximumCallVolatage(parseFloat(s[30]));
		mParam.setMinimumCellTemperature(parseFloat(s[31]));
		mParam.setMaximumCellTemperature(parseFloat(s[32]));
		mParam.setEvents(Integer.parseInt(s[33]));
		mParam.setNetworkMode(Integer.parseInt(s[34]));
		mParam.setNetworkInformation(Integer.parseInt(s[35]));
		mParam.setNumberOfCells(Integer.parseInt(s[36]));
		mParam.setSw_version(s[37]);
		mParam.setBatteryVoltageCum(s[38]);
		mParam.setBattery_type((s[39]));
		mParam.setCreatedDate(new Timestamp(date.getTime()));
		mParam.setOrgId(Float.parseFloat(s[41]));
		mParam.setMobilisation_status((s[42]));
		mParam.setBoardTemperatureAfe2(Float.parseFloat(s[43]));
		mParam.setTcu_date_ist(new Timestamp(date.getTime()));
		mParam.setTcu_date1(new Timestamp(date.getTime()));
		mParam.setTcu_date_ist1(new Timestamp(date.getTime()));
//            try {
//                mParam.setTcu_date_ist(commonUtil.convertSimpleDateFormat(commonUtil.getModifiedTimeinIST(b)));
//                b.setTcu_date(commonUtil.getModifiedTimeinIST(mParam));
//                b.setTcu_date_ist1(commonUtil.convertSimpleDateFormat(commonUtil.getModifiedNewTimeinIST(b)));
//                b.setTcu_date1(commonUtil.getModifiedNewTimeinIST(b));
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }
		cellVoltageAndTemperatureMapper(cellVoltages, cellTemperatures, s, mParam);

		if (s.length > 75) {

			String var = s[s.length - 1].replaceAll("[|#]", "");
			mParam.setBms_sequence_number(Long.parseLong(var));
			mParam.setBms_triggered_tcu_reset_reason((s[s.length - 2]));
			mParam.setNetwork_information_1(Float.parseFloat(s[s.length - 3]));
			mParam.setEvents_1(Integer.parseInt(s[s.length - 4]));
			mParam.setErrors_1(Integer.parseInt(s[s.length - 5]));
			mParam.setPlatform(Float.parseFloat(s[s.length - 6]));
			mParam.setBms_reset_reason(Integer.parseInt(s[s.length - 7]));
			mParam.setBms_reset_count(Integer.parseInt(s[s.length - 8]));
			////////////

			// String var = s[s.length - 1].replaceAll("[|#]", "");
			if (mParam.getCellTemperature().equals(s[s.length - 10])) {
				mParam.setBmsConfigurationVersion(Integer.parseInt(s[s.length - 9]));
			} else {
				mParam.setTcuEvents(Integer.parseInt(s[s.length - 9]));
				mParam.setResetCounter(Integer.parseInt(s[s.length - 10]));
				// mParam.setBoardTemperatureAfe2(Float.parseFloat(s[s.length - 11]));
				mParam.setNumberOfBoardTemperature(Integer.parseInt(s[s.length - 12]));
				for (int i = 9; i <= 13; i++) {
					strengthOfSatellites.add(Float.parseFloat(s[s.length - 12 - i]));
				}
				mParam.setStrengthOfTop5Satellites(strengthOfSatellites.toString());
				mParam.setNoOfSatellites(Float.parseFloat(s[s.length - 18]));
				mParam.setTamperedSwCount(s[s.length - 19]);
				mParam.setTcuErrorInfo(s[s.length - 20]);
				mParam.setGsmCsq(s[s.length - 21]);
				mParam.setTcuDate(new Timestamp(date.getTime()));
				mParam.setTcuTime(s[s.length - 23]);
				mParam.setBmsConfigurationVersion(Float.parseFloat(s[s.length - 24]));
			}

		}

		else {

			String var = s[s.length - 1].replaceAll("[|#]", "");
			if (mParam.getCellTemperature().equals(s[s.length - 2])) {
				mParam.setBmsConfigurationVersion(Integer.parseInt(var));
			} else {
				mParam.setTcuEvents(Integer.parseInt(var));
				mParam.setResetCounter(Integer.parseInt(s[s.length - 2]));
				// mParam.setBoardTemperatureAfe2(Float.parseFloat(s[s.length - 3]));
				mParam.setNumberOfBoardTemperature(Integer.parseInt(s[s.length - 4]));
				for (int i = 1; i <= 5; i++) {
					strengthOfSatellites.add(Float.parseFloat(s[s.length - 4 - i]));
				}
				mParam.setStrengthOfTop5Satellites(strengthOfSatellites.toString());
				mParam.setNoOfSatellites(Float.parseFloat(s[s.length - 10]));
				mParam.setTamperedSwCount(s[s.length - 11]);
				mParam.setTcuErrorInfo(s[s.length - 12]);
				mParam.setGsmCsq(s[s.length - 13]);
				mParam.setTcuDate(new Timestamp(date.getTime()));
				mParam.setTcuTime(s[s.length - 15]);
				mParam.setBmsConfigurationVersion(Integer.parseInt(s[s.length - 16]));
			}

		}

		log.info(String.valueOf(mParam));
		return mParam;

	}

	private static void cellVoltageAndTemperatureMapper(ArrayList<String> cellVoltages,
			ArrayList<String> cellTemperatures, String[] s, BellurbisUSABatteryData mParam) {
		Integer maxIndexOfCellVoltages = 36 + 16;
		int arrayIndexOfCellVoltages = 0;
		float sumOfVoltage = 0;
		float sumOfTemperature = 0;
		int i;
		for (i = 37; i <= maxIndexOfCellVoltages; i++) {
			cellVoltages.add(s[i]);
			arrayIndexOfCellVoltages = i;
			sumOfVoltage = sumOfVoltage + Float.parseFloat(s[i]);
		}
		// mParam.setSumOfCellVoltages(sumOfVoltage);
		mParam.setNumberOfTemperatureSensors(Integer.parseInt(s[arrayIndexOfCellVoltages + 1]));
		mParam.setCellVoltages(cellVoltages.toString());
		int maxOfCellTemperaturesCount = arrayIndexOfCellVoltages + 1 + 5; ////// >>>>>>>>>>>>>>> HARD Code Value for 5
																			////// Cell Temperature
		i = arrayIndexOfCellVoltages + 2;
		for (; i <= maxOfCellTemperaturesCount; i++) {
			cellTemperatures.add(s[i]);
			sumOfTemperature += Float.parseFloat(s[i]);
		}
		// mParam.setSumOfCellTemperature(sumOfTemperature);
		mParam.setCellTemperature(cellTemperatures.toString());
	}

}
