package com.bellurbis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BmsDmpApplication {

	public static void main(String[] args) {
		SpringApplication.run(BmsDmpApplication.class, args);
	}

}
