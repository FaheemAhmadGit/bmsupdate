package com.bellurbis.service.api;

import com.bellurbis.model.BellurbissIndoreBaatteryData;

public interface BellurbissIndoreBatteryServiceApi {
	 public BellurbissIndoreBaatteryData saveBatteryInfo(BellurbissIndoreBaatteryData batteryEntity);
}
