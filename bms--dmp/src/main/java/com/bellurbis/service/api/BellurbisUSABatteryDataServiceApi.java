package com.bellurbis.service.api;

import org.springframework.stereotype.Component;

import com.bellurbis.model.BellurbisUSABatteryData;
@Component
public interface ExicomBounceBatteryDataServiceApi {
	public BellurbisUSABatteryData saveBatteryInfo(BellurbisUSABatteryData batteryEntity);

}
