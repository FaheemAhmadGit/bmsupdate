package com.bellurbis.service.api;

import com.bellurbis.model.BellubissGurugramBatteryData;

public interface BellurbissGurugramBatteryDataServiceApi {
    public BellubissGurugramBatteryData saveBatteryInfo(BellubissGurugramBatteryData batteryEntity);

}
