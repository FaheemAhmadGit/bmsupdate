package com.bellurbis.service.api;

import java.util.List;

import org.springframework.stereotype.Component;

import com.bellurbis.model.BatteryEntity;


@Component
public interface BatteryDataServiceApi {

    public BatteryEntity saveBatteryInfo(BatteryEntity batteryEntity);


  }
