package com.bellurbis.service.impl;


import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bellurbis.entity.AssetEntity;
import com.bellurbis.model.BatteryEntity;
import com.bellurbis.repo.AssetRepo;
import com.bellurbis.repo.BatteryRepo;
import com.bellurbis.service.api.BatteryDataServiceApi;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;


@Service
@Slf4j
public class BatteryDataServiceImpl implements BatteryDataServiceApi {

	@Autowired
    private BatteryRepo batteryRepo;

    private AssetRepo assetRepo;
    @Override
    public BatteryEntity saveBatteryInfo(BatteryEntity batteryEntity) {
        log.info("batteryData saved");
        return batteryRepo.save(batteryEntity);

    }

    }

