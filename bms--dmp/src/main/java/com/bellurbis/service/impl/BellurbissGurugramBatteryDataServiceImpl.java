package com.bellurbis.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bellurbis.entity.AssetEntity;
import com.bellurbis.model.BellubissGurugramBatteryData;
import com.bellurbis.repo.AssetRepo;
import com.bellurbis.repo.BellurbissGurugramBatteryDataRepo;
import com.bellurbis.service.api.BellurbissGurugramBatteryDataServiceApi;

import lombok.extern.slf4j.Slf4j;
@Slf4j
@Service
public class BellurbissGurugramBatteryDataServiceImpl implements BellurbissGurugramBatteryDataServiceApi {

	@Autowired
	private AssetRepo assetRepo;
	@Autowired
	private BellurbissGurugramBatteryDataRepo rbmlBatteryDataRepo;	
	@Override
	public BellubissGurugramBatteryData saveBatteryInfo(BellubissGurugramBatteryData batteryEntity) {
//			   Optional<AssetEntity> existByImeiNo = assetRepo.findByImeiNoAndBinIgnoreCase(batteryEntity.getDeviceId(), batteryEntity.getBin());
//		        if (batteryEntity.getBin().startsWith("00000000000")) {
//		            System.out.println("Zero Bin Found!!!" + batteryEntity.getBin());
//		            return null;
//		        }
//		        if (existByImeiNo.isPresent()) {
//		            log.info("Data saved org = {} and bin={} on date ={}", existByImeiNo.get().getOrgId(), existByImeiNo.get().getBin(), batteryEntity.getCreatedDate());
////		            batteryEntity.setBin(existByImeiNo.get().getBin());
//		            batteryEntity.setOrgId(existByImeiNo.get().getOrgId());
//		        }
		        log.info("batteryData saved");
		        return rbmlBatteryDataRepo.save(batteryEntity);

		
	}
	
	
}
