package com.bellurbis.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bellurbis.entity.AssetEntity;
import com.bellurbis.model.BellurbissBatteryData;
import com.bellurbis.repo.AssetRepo;
import com.bellurbis.repo.BellurbissBatteryDataRepo;
import com.bellurbis.service.api.BellurbissBatteryDataServiceApi;

import lombok.extern.slf4j.Slf4j;
@Service
@Slf4j
public class BellurbisBatteryServiceImpl implements BellurbissBatteryDataServiceApi {
	@Autowired
    private AssetRepo assetRepo;

	@Autowired
	private BellurbissBatteryDataRepo exicomBatteryDataRepo;
	@Override
	public BellurbissBatteryData saveBatteryInfo(BellurbissBatteryData batteryEntity) {
		   Optional<AssetEntity> existByImeiNo = assetRepo.findByImeiNoAndBinIgnoreCase(batteryEntity.getDeviceId(), batteryEntity.getBin());
	        if (batteryEntity.getBin().startsWith("00000000000")) {
	            System.out.println("Zero Bin Found!!!" + batteryEntity.getBin());
	            return null;
	        }
//	        if (existByImeiNo.isPresent()) {
//	            log.info("Data saved org = {} and bin={} on date ={}", existByImeiNo.get().getOrgId(), existByImeiNo.get().getBin(), batteryEntity.getCreatedDate());
////	            batteryEntity.setBin(existByImeiNo.get().getBin());
//	            batteryEntity.setOrgId(existByImeiNo.get().getOrgId());
//	        }
	        log.info("batteryData saved");
	        return exicomBatteryDataRepo.save(batteryEntity);

	}

	
}
