package com.bellurbis.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bellurbis.entity.AssetEntity;
import com.bellurbis.model.BellurbisUSABatteryData;
import com.bellurbis.repo.AssetRepo;
import com.bellurbis.repo.BellurbissUSABatteryDataRepo;
import com.bellurbis.service.api.ExicomBounceBatteryDataServiceApi;

import lombok.extern.slf4j.Slf4j;
@Service
@Slf4j
public class ExicomBounceBatteryDataServiceImpl implements ExicomBounceBatteryDataServiceApi {
	@Autowired
    private AssetRepo assetRepo;
    @Autowired
	private BellurbissUSABatteryDataRepo exicomBounceBatteryDatarepo;
	@Override
	public BellurbisUSABatteryData saveBatteryInfo(BellurbisUSABatteryData batteryEntity) {
//		   Optional<AssetEntity> existByImeiNo = assetRepo.findByImeiNoAndBinIgnoreCase(batteryEntity.getDeviceId(), batteryEntity.getBin());
//	        if (batteryEntity.getBin().startsWith("00000000000")) {
//	            System.out.println("Zero Bin Found!!!" + batteryEntity.getBin());
//	            return null;
//	        }
//	        if (existByImeiNo.isPresent()) {
//	            log.info("Data saved org = {} and bin={} on date ={}", existByImeiNo.get().getOrgId(), existByImeiNo.get().getBin(), batteryEntity.getCreatedDate());
////	            batteryEntity.setBin(existByImeiNo.get().getBin());
//	            batteryEntity.setOrgId(existByImeiNo.get().getOrgId());
//	        }
	        log.info("ExicomBouncebatteryData saved");
	        return exicomBounceBatteryDatarepo.save(batteryEntity);

	}

	
}
