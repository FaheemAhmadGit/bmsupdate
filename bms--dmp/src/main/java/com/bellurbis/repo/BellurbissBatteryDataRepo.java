package com.bellurbis.repo;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bellurbis.model.BellurbissBatteryData;

import java.util.Date;
import java.util.List;
import java.util.Optional;


@Repository
public interface BellurbissBatteryDataRepo extends JpaRepository<BellurbissBatteryData, Integer> {


}


