package com.bellurbis.repo;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.persistence.criteria.CriteriaBuilder.In;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bellurbis.entity.AssetEntity;


@Repository
public interface AssetRepo extends JpaRepository<AssetEntity, Integer> {

    public Optional<AssetEntity> findByOrgIdAndId(int orgId, int id);

    public Page<AssetEntity> findByOrgIdAndActive(Pageable page, int orgId, boolean activeYn);

    public Page<AssetEntity> findByOrgIdAndActiveAndBinLikeOrOrgIdAndActiveAndSearchTagsLike(Pageable page, int orgId, boolean activeYn, String bin, int org, boolean active,String imei);

    public Page<AssetEntity> findByOrgIdAndActiveAndBinLikeOrTcuFwversionLikeOrOrgIdAndActiveAndSearchTagsLike(Pageable page, int orgId, boolean activeYn, String bin, String tcuFwversion,  int org, boolean active,String imei);

    public Page<AssetEntity> findByOrgIdAndActiveAndBinLikeOrOrgIdAndActiveAndSearchTagsLikeOrOrgIdAndActiveAndTcuFwversionLike(Pageable page, int orgId, boolean activeYn, String bin, int org, boolean active, String imei, int orgT, boolean activeT,String imeiT);

    public Page<AssetEntity> findByActiveAndBinLike(Pageable page, boolean activeYn, String bin);

    public Page<AssetEntity> findByActiveTrue(Pageable page);

    public Page<AssetEntity> findByOrgId(Pageable page, int orgId);

    public Optional<AssetEntity> findById(Integer id);

    public Boolean existsByBin(String bin);

    public Boolean existsByImeiNo(long imei);

    public Boolean existsByImeiNoAndOrgId(long imei, Integer orgId);

    public Optional<AssetEntity> findByImeiNo(long imei);

    public Optional<AssetEntity> findByImeiNoAndBinIgnoreCase(long imeiNo, String bin);

    public Optional<AssetEntity> findByImeiNoAndOrgId(long imei, Integer orgId);

    public Optional<AssetEntity> findByImeiNoAndBinIgnoreCaseAndOrgId(long imeiNo, String bin, Integer orgId);

    public void deleteById(Integer id);

    long countByOrgId(int id);

    public AssetEntity findAssetByImeiNoAndOrgId(Long imeiNo, Integer orgId);

    @Query(value = "SELECT DISTINCT bms FROM t_asset", nativeQuery = true)
    public List<String> get_distinct_version_of_bms();

    @Query(value = "SELECT DISTINCT tcu FROM t_asset", nativeQuery = true)
    public List<String> get_distinct_version_of_tcu();

    public List<AssetEntity> findByOrgId(Integer orgId);

    List<AssetEntity> findByBinStartingWith(String bin);

    @Query(value = "select "
            + "tso.name as name,"
            + "tso.org_id as orgId,"
            + "tso.sequence_order as sequenceOrder,"
            + "tso.master_id as masterId,"
            + "org.topic as topic "
            + "from t_asset ta "
            + "inner join t_data_packet_sequence_order tso on ta.org_id=tso.org_id "
            + "inner join t_organisation as org on org.id=ta.org_id "
            + "where ta.bin =?", nativeQuery = true)
    //@Query("select orgId from AssetEntity ae where ae.bin=:bin")
    public List<Map<String, Object>> getSequencedDatafields(@Param("bin") String bin);

    /* @Query(value = "select bd.") */


    @Query(value = "select t_organisation.topic,t_data_packet_sequence_order.name,t_data_packet_sequence_order.sequence_order,t_data_packet_sequence_order.column_name from t_organisation " +
            "inner join t_data_packet_sequence_order on t_data_packet_sequence_order.org_id=t_organisation.id " +
            "where t_organisation.id=?", nativeQuery = true)
    public List<Map<String, Object>> getTopicAndSequenceWithOrgId(@Param("orgId") Integer orgId);

    @Query(value = "SELECT COUNT(DISTINCT imei_no) from t_asset where org_id = ? and t_asset.modified_date   > TIMESTAMP 'today'", nativeQuery = true)
    public List<Map<String, Object>> getTotalActive(int orgId);

    @Query(value = "SELECT bms,imei_no,model_id,pdu,tcu FROM public.t_asset where imei_no=?;", nativeQuery = true)
    public List<Map<String, Object>> getAssetTreeData(long imeiNo);

    public Boolean existsByImeiNoAndOrgId(Long imeiNo, Integer orgId);


    @Query(value = "SELECT id,tcu,bms,bms_config_file_version,imei_no,bin FROM public.t_asset where org_id=? order by id asc;", nativeQuery = true)
    public List<Map<String, Object>> findByOrgIdselected(Integer orgId);



    @Query(value = "select * from  public.t_asset  where org_id = ?2 and (imei_no::text  LIKE  ?3 or bin LIKE ?3)",
            countQuery = "SELECT count(*) FROM public.t_asset",
            nativeQuery = true)
    public Page<AssetEntity> findByOrgIdAndActiveAndBinLikeQuery(Pageable page, int orgId, String bin);

    public List<AssetEntity> findByActiveTrue();
}


