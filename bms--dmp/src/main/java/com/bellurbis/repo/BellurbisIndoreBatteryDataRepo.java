package com.bellurbis.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bellurbis.model.BellurbissIndoreBaatteryData;

@Repository
public interface BellurbisIndoreBatteryDataRepo extends JpaRepository<BellurbissIndoreBaatteryData, Integer>{

}
