package com.bellurbis.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bellurbis.model.BellubissGurugramBatteryData;

@Repository
public interface BellurbissGurugramBatteryDataRepo extends JpaRepository<BellubissGurugramBatteryData, Integer> {

}
