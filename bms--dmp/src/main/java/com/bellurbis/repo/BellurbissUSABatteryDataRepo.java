package com.bellurbis.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bellurbis.model.BellurbisUSABatteryData;

@Repository
public interface BellurbissUSABatteryDataRepo extends JpaRepository<BellurbisUSABatteryData, Integer> {

}
