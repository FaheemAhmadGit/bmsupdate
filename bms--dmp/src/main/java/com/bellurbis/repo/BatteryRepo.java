package com.bellurbis.repo;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bellurbis.model.BatteryEntity;


@Repository
public interface BatteryRepo extends JpaRepository<BatteryEntity, Integer> {

    Page<BatteryEntity> findAll(Pageable pageable);

    List<BatteryEntity> findByCreatedDateBetween(Date start, Date end);

    Optional<BatteryEntity> findByDeviceId(long imei);
}
