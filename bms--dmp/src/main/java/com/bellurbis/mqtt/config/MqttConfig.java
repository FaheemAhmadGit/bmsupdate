package com.bellurbis.mqtt.config;

public abstract class MqttConfig {

    protected Boolean withUserNamePass = true;

    protected final String broker = "Stagingbn.bt.exicom.in";
    protected final int qos = 1;
    protected Boolean hasSSL = false; /* By default SSL is disabled */
    protected Integer port = 1883; /* Default port */
    protected final String userName = "exicom.mqtt";
    protected final String password = "Secure}123";

    protected final String TCP = "tcp://";
    protected final String SSL = "ssl://";

    /**
     * Custom Configuration
     *
     * @param broker
     * @param port
     * @param ssl
     * @param withUserNamePass
     */
    protected abstract void config(String broker, Integer port, Boolean ssl, Boolean withUserNamePass);

    /**
     * Default Configuration
     */
    protected abstract void config();

}
