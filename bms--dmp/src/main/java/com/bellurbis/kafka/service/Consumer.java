package com.bellurbis.kafka.service;

import java.sql.Timestamp;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

import com.bellurbis.common.CommonUtil;
import com.bellurbis.model.BatteryEntity;
//import com.bellurbis.model.ErrorModel;
import com.bellurbis.model.BellurbissBatteryData;
import com.bellurbis.model.BellurbisUSABatteryData;
//import com.bellurbis.model.MessageParam51;
import com.bellurbis.model.PayloadParser;
//import com.bellurbis.model.PublishDataToJioBp;
import com.bellurbis.model.BellubissGurugramBatteryData;
import com.bellurbis.model.BellurbissIndoreBaatteryData;
//import com.bellurbis.model.WarningModel;
//import com.bellurbis.repo.ErrorRepo;
import com.bellurbis.repo.BellurbissUSABatteryDataRepo;
//import com.bellurbis.repo.WarningRepo;
import com.bellurbis.service.api.BatteryDataServiceApi;
import com.bellurbis.service.impl.BellurbisBatteryServiceImpl;
import com.bellurbis.service.impl.BellurbisUSABatteryDataServiceImpl;
import com.bellurbis.service.impl.BellurbissGurugramBatteryDataServiceImpl;
import com.bellurbis.service.impl.BellurbissIndoreBatteryDataServiceImpl;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class Consumer {

	private static final String TOPIC1 = "inComingTopicForBellurbissGurugramBatteryData";
	private static final String TOPIC2 = "inComingTopicForBellurbissIndoreBatteryData";
	private static final String TOPIC3 = "inComingTopicForBellurbissBatteryData";
	private static final String TOPIC4 = "inComingTopicForBellurbissUSABatteryData";
	private static final String TOPIC5 = "inComingTopicForAllBatteryData";


	@Autowired
	private BatteryDataServiceApi batteryService;

	@Autowired
	private DataSetterDbWrapper dataSetterDbWrapper;

	@Autowired
	private BellurbisBatteryServiceImpl exicombatteryServiceImpl;

	@Autowired
	private BellurbissGurugramBatteryDataServiceImpl rbmlBatteryDataServiceImpl;
	private BellurbisUSABatteryDataServiceImpl bounceBatteryDataServiceImpl;
	@Autowired
	private BellurbissIndoreBatteryDataServiceImpl rrlServiceImpl;

	@Autowired
	private BellurbissUSABatteryDataRepo bounceBatteryDataRepo;

	@Autowired
	private CommonUtil commonUtil;

	@KafkaListener(topics = TOPIC1, groupId = "one")
	public void kafkaMasterDataOne(@Payload String data, @Header("kafka_receivedPartitionId") int partition,
								   @Header("kafka_receivedTopic") String topic, @Header("kafka_receivedTimestamp") long ts) throws ParseException {

		log.error("Error:::::::::::::::::::::::::::::::::::::::::");
		{
			log.info("consume_topicBellurbissGurugramBatteryData::::::::", data);
			BellubissGurugramBatteryData batteryData = PayloadParser.stringParserMapper(data,topic);
			this.rbmlBatteryDataServiceImpl.saveBatteryInfo(batteryData);
			log.info("saveData", topic);
		}
	}

	@KafkaListener(topics = TOPIC2, groupId = "two")
	public void kafkaMasterDataTwo(@Payload String data, @Header("kafka_receivedPartitionId") int partition,
								   @Header("kafka_receivedTopic") String topic, @Header("kafka_receivedTimestamp") long ts) {


		{
			BellurbissIndoreBaatteryData batteryData = PayloadParser.stringParserMapperForRRL(data, topic);

			log.info("consume_topicBellurbisIndoreBatteryData::::::", data);
			this.rrlServiceImpl.saveBatteryInfo(batteryData);
			log.info("SaveDataintoRRl");
		}

	}

	@KafkaListener(topics = TOPIC3, groupId = "three")
	public void kafkaMasterDataThree(@Payload String data, @Header("kafka_receivedPartitionId") int partition,
									 @Header("kafka_receivedTopic") String topic, @Header("kafka_receivedTimestamp") long ts) {
		{
			BellurbissBatteryData batteryData = PayloadParser.stringParserMapperForExicom(data, topic);

			log.info("consume_topicBellurbisBatteryData:::::::", data);
			this.exicombatteryServiceImpl.saveBatteryInfo(batteryData);
		}

	}


	@KafkaListener(topics = TOPIC5, groupId = "five")
	public void kafkaMasterDataFive(@Payload String data, @Header(KafkaHeaders.RECEIVED_MESSAGE_KEY) String key,
									@Header("kafka_receivedPartitionId") int partition,
									@Header("kafka_receivedTopic") String topic, @Header("kafka_receivedTimestamp") long ts) {
		BatteryEntity batteryEntity = PayloadParser.stringParserMapperForBatteryEntity(data, topic);

		log.info("consume_topicAllBatteryData", data);
		this.batteryService.saveBatteryInfo(batteryEntity);

	}

	@KafkaListener(topics = TOPIC5, groupId = "six")
	public void kafkaMasterDataSix(@Payload String data, @Header("kafka_receivedPartitionId") int partition,
								   @Header("kafka_receivedTopic") String topic, @Header("kafka_receivedTimestamp") long ts,
								   @Header(KafkaHeaders.RECEIVED_MESSAGE_KEY) String key) {
		data.split(data);
		BatteryEntity batteryEntity = PayloadParser.stringParserMapperForBatteryEntity(data, topic);
		log.info("consume_topicForAssetEntity", data);
		this.dataSetterDbWrapper.assetProcessing(batteryEntity);

	}

}