package com.bellurbis.kafka.service;

import java.util.Date;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.bellurbis.entity.AssetEntity;
import com.bellurbis.model.BatteryEntity;
import com.bellurbis.repo.AssetRepo;


@Component
public class DataSetterDbWrapper {
    @Autowired
    private AssetRepo assetRepo;


    private static final Logger log = LoggerFactory.getLogger(DataSetterDbWrapper.class);

    private static final String Asset_event = "update_asset_activity_23";

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplateClient;


    
    public BatteryEntity assetProcessing(BatteryEntity b) {
        if (b.getBin().startsWith("00000000000")) {
            log.error(b.getBin());
            return null;
        }
        Optional<AssetEntity> existByImeiNo = assetRepo.findByImeiNoAndBinIgnoreCase(b.getDeviceId(), b.getBin());
        if (existByImeiNo.isPresent()) {
            AssetEntity updateAssetInformation = updateAssetInformation(b);
            log.info("Asset Update for ={}  on = {}", b.getDeviceId(), b.getOrgId().toString());
            kafkaTemplateClient.send(Asset_event, "update", updateAssetInformation.toString());

        } 
        else {
            AssetEntity setAssetInformation = setAssetInformation(b);
        }
        return null;
    }


    @Async
    public AssetEntity setAssetInformation(BatteryEntity b) {
        AssetEntity assets = new AssetEntity();
        assets.setOrgId(1);
        assets.setBin(b.getBin());
        assets.setBmsBuildNumber(b.getBmsBuildNumber());
        assets.setBmsConfigurationVersion(b.getBmsConfigurationVersion());
        assets.setTcuFwversion(b.getTcuFwversion());
        assets.setModelId("1");
        assets.setModelVersionId(1);
        assets.setImeiNo(b.getDeviceId());
        assets.setActive(true);
        assets.setLatitude(b.getLatitude());
        assets.setLongitude(b.getLongitude());
        assets.setAssetCatagory("battery");
        assets.setCreatedDate(new Date());
        assets.setModifiedDate(new Date());
        assets.setLastUpdateDate(new Date());
        assets.setData04(0);
     //   assets.setData05(0);
        assets.setSearchTags(Long.toString(b.getDeviceId()));
        System.err.println(assets.toString());
        return assetRepo.save(assets);
    }


    @Async
    public AssetEntity updateAssetInformation(BatteryEntity b) {
//        Optional<AssetEntity> existByImeiNo = assetRepo.findByImeiNo(b.getDeviceId());
        Optional<AssetEntity> existByImeiNo = assetRepo.findByImeiNoAndBinIgnoreCase(b.getDeviceId(), b.getBin());
        if (existByImeiNo.isPresent()) {
            AssetEntity existAssetEntity = existByImeiNo.get();
            existAssetEntity.setBmsBuildNumber(b.getBmsBuildNumber());
//            existAssetEntity.setBin(b.getBin());
            existAssetEntity.setBmsConfigurationVersion(b.getBmsConfigurationVersion());
            existAssetEntity.setTcuFwversion(b.getTcuFwversion());
            existAssetEntity.setLatitude(b.getLatitude());
            existAssetEntity.setLongitude(b.getLongitude());
            existAssetEntity.setLastUpdateDate(new Date());
            existAssetEntity.setModifiedDate(new Date());
            return assetRepo.save(existAssetEntity);
        }
        return null;
    }




}
