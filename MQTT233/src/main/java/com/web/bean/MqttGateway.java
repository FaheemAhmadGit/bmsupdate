package com.web.bean;

import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.integration.mqtt.support.MqttHeaders;
import org.springframework.messaging.handler.annotation.Header;

import com.google.gson.JsonObject;

@MessagingGateway(defaultRequestChannel = "mqttOutboundChannel")
public interface MqttGateway {

	void senToMqtt(String data, @Header(MqttHeaders.TOPIC) String topic);
    void recieveToMqtt(String data);
	 void sendtoMqtt(String data);
	void sendtoMqtt(JsonObject convertObject);
}
