package com.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.web.bean.MqttGateway;
import com.web.model.MqttModel;

@SpringBootApplication
@EnableScheduling
public class MqttApplication implements Runnable {
@Autowired
private MqttGateway gateway;
	public static void main(String[] args) {
		SpringApplication.run(MqttApplication.class, args);
	}

	@Override
	public void run() {
		MqttModel model=new MqttModel();
		gateway.senToMqtt(model);
	}

}
