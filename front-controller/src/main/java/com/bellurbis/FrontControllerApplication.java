package com.bellurbis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.core.task.TaskExecutor;

@SpringBootApplication
public class FrontControllerApplication extends SpringBootServletInitializer{

//	@Autowired
//	Runnable MessageListenerForExicom;
//	@Autowired
//	Runnable MessageListenerForExicomBounce;
//	@Autowired
//	Runnable MessageListenerRBML;
//	@Autowired
//	Runnable MessageListenerRRL;
	@Autowired
	Runnable MessageListener;




	
	public static void main(String[] args) {
		SpringApplication.run(FrontControllerApplication.class, args);
	}
//	@Bean
//	public CommandLineRunner schedulingRunner1(TaskExecutor executor) {
//		return new CommandLineRunner() {
//			public void run(String... args) throws Exception {
//				executor.execute(MessageListenerForExicom);
//			}
//		};
//	}
//	@Bean
//	public CommandLineRunner schedulingRunner2(TaskExecutor executor) {
//		return new CommandLineRunner() {
//			public void run(String... args) throws Exception {
//				executor.execute(MessageListenerForExicomBounce);
//			}
//		};
//	}
//	@Bean
//	public CommandLineRunner schedulingRunner3(TaskExecutor executor) {
//		return new CommandLineRunner() {
//			public void run(String... args) throws Exception {
//				executor.execute(MessageListenerRBML);
//			}
//		};
//	}
//	@Bean
//	public CommandLineRunner schedulingRunner4(TaskExecutor executor) {
//		return new CommandLineRunner() {
//			public void run(String... args) throws Exception {
//				executor.execute(MessageListenerRRL);
//			}
//		};
//	}
	@Bean
	public CommandLineRunner schedulingRunner5(TaskExecutor executor) {
		return new CommandLineRunner() {
			public void run(String... args) throws Exception {
				executor.execute(MessageListener);
			}
		};
	}
}
