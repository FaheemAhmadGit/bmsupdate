package com.bellurbis.kafka.topic.config;

import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.KafkaAdminClient;
import org.apache.kafka.clients.admin.ListTopicsResult;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.ExecutionException;
@Qualifier
@Configuration
public class KakfaConfiguration {
	 @Value("${app.kafka.topic1}")
	private String topic1;
	 @Value("${app2.kafka.topic2}")
		private String topic2;
	 @Value("${app3.kafka.topic3}")
		private String topic3;
	 @Value("${app4.kafka.topic4}")
		private String topic4;
	 @Value("${app5.kafka.topic5}")
		private String topic5;
		 
	
	@Bean
    public NewTopic topic1() {
        return new NewTopic(topic1, 2, (short) 1);
    }
	@Bean
    public NewTopic topic2() {
        return new NewTopic(topic2, 2, (short) 1);
    }
	@Bean
    public NewTopic topic3() {
        return new NewTopic(topic3, 2, (short) 1);
    }
	@Bean
    public NewTopic topic4() {
        return new NewTopic(topic4, 2, (short) 1);
    }
	@Bean
    public NewTopic topic5() {
        return new NewTopic(topic5, 2, (short) 1);
    }


}
