
package com.bellurbis.mqtt.config;
/*
*this is MQTT server configuration	file
	
*/
public abstract class MqttConfig {
	/*
	 *  Stagingbn.bt.exicom.in is coming from client side
		
	*/	
	//protected final String broker = "Stagingbn.bt.exicom.in";
	protected final String broker = "tcp://localhost:1883";
    protected final int qos = 1;
    //protected Boolean hasSSL = false; /* By default SSL is disabled */
   // protected Integer port = 1883; /* Default port */
//	/*
//	 * userName = "exicom.mqtt" is clientName
//	*/
//   // protected final String userName = "exicom.mqtt";
//	/*
//	 * password = "Secure}123" is client Password
//	
//	*/ 
//   // protected final String password = "Secure}123";
  //  protected final String TCP = "tcp://";
    protected final String SSL = "ssl://";

    /**
     * Custom Configuration
     *
     * @param broker
     * @param port
     * @param ssl
     */
   // protected abstract void config(String broker, Integer port, Boolean ssl, Boolean withUserNamePass);

    /**
     * Default Configuration
     */
    protected abstract void config();


}
