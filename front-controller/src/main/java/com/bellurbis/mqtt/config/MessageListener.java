package com.bellurbis.mqtt.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.bellurbis.mqtt.subscriber.MqttSubscriberApi;

@Component
public class MessageListener implements Runnable {

	@Autowired
	MqttSubscriberApi subscriber;

	@Value("${app.mqtt.subscribe1}")
	private String subscribe1;
	@Value("${app.mqtt.subscribe2}")
	private String subscribe2;
	@Value("${app.mqtt.subscribe3}")
	private String subscribe3;

	@Override
	public void run() {
		while (true) {
//			subscriber.subscribeMessage("bellurbissGurugram");
//			subscriber.subscribeMessage("bellirbisIndore");
			subscriber.subscribeMessage(subscribe1);
			subscriber.subscribeMessage(subscribe2);
			subscriber.subscribeMessage(subscribe3);
		}

	}

}
