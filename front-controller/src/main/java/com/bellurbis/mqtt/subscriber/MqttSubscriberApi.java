package com.bellurbis.mqtt.subscriber;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;


public interface MqttSubscriberApi {
	
	
	/**
	 * Subscribe message
	 * 
	 * @param topic
	 */
	public void subscribeMessage(String topic);

	/**
	 * Disconnect MQTT Client
	 */
	public void disconnect();

}
