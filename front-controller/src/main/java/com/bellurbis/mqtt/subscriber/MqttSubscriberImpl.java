package com.bellurbis.mqtt.subscriber;

import static java.lang.Float.parseFloat;
import static java.lang.Integer.parseInt;

import java.sql.Timestamp;
import java.util.*;
import java.util.concurrent.TimeUnit;

import org.apache.kafka.clients.producer.ProducerRecord;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.event.EventListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.event.NonResponsiveConsumerEvent;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

import com.bellurbis.mqtt.config.MqttConfig;

/*
 *MqttSubscriberImpl is used for subscribe the topic form client side
 *
 */
@Component
public class MqttSubscriberImpl extends MqttConfig implements MqttCallback, MqttSubscriberApi {

	@Autowired
	private KafkaTemplate<String, String> kafkaTemplateClient;

	@Value("${app.kafka.topic1}")
	private String allBetteryRBML;
	@Value("${app2.kafka.topic2}")
	private String BetteryTopicRRL;
	@Value("${app3.kafka.topic3}")
	private String allBetteryExicom;
	@Value("${app4.kafka.topic4}")
	private String allBetteryExicomBounce;
	@Value("${app5.kafka.topic5}")
	private String allBetteryTopic;

	//	@Value("${app.mqtt.subscribe1}")
//	private String subscribeExicomBattery;
	@Value("${app.mqtt.subscribe2}")
	private String subscribeRBMLBattery;
	@Value("${app.mqtt.subscribe3}")
	private String subscribeExicomBounce;
//	@Value("${app.mqtt.subscribe4}")
//	private String subscriberrl;

	private static int retryAttempt = 1;

	private String brokerUrl = null;
	final private String colon = ":";
	final private String clientId = UUID.randomUUID().toString();

	private MqttClient mqttClient = null;
	private MqttConnectOptions connectionOptions = null;
	private MemoryPersistence persistence = null;

	private static final Logger logger = LoggerFactory.getLogger(MqttSubscriberImpl.class);

	public MqttSubscriberImpl() {
		logger.info("I am MqttSub impl");
		this.config();
	}

	@Override
	public void connectionLost(Throwable cause) {
		logger.info("Connection Lost" + cause);
		logger.info("I am MqttSub impl");
		this.config();
	}

	/*
	 * after Subscribing messageArrived is used for send to the kafka server
	 * KafkaTemplate
	 *
	 */
	@Override
	public void messageArrived(String mqttTopic, MqttMessage mqttMessage) throws Exception {
		int part = new Random().nextInt(30);// used to generate random number
		String finalTopicName = mqttTopic + "--" + part;
		String message=mqttMessage.toString();
		ProducerRecord<String, String> recordMasterDb = new ProducerRecord<>(allBetteryTopic, finalTopicName,
				new String(mqttMessage.getPayload()));
		ProducerRecord<String, String> recordMasterDb1 = new ProducerRecord<>(BetteryTopicRRL, finalTopicName,
				new String(mqttMessage.getPayload()));
		ProducerRecord<String, String> recordMasterDb2 = new ProducerRecord<>(allBetteryRBML, finalTopicName,
				new String(mqttMessage.getPayload()));
		ProducerRecord<String, String> recordMasterDb3 = new ProducerRecord<>(allBetteryExicom, finalTopicName,
				new String(mqttMessage.getPayload()));
		ProducerRecord<String, String> recordMasterDb4 = new ProducerRecord<>(allBetteryExicomBounce, finalTopicName,
				new String(mqttMessage.getPayload()));

		ListenableFuture<SendResult<String, String>> future1 = kafkaTemplateClient.send(recordMasterDb);
		future1.addCallback(new ListenableFutureCallback<SendResult<String,String>>() {

			@Override
			public void onSuccess(SendResult<String, String> result) {
				logger.info("sent message = {} with offset = {}", result, result.getRecordMetadata().offset());

			}

			@Override
			public void onFailure(Throwable ex) {
				// TODO Auto-generated method stub

			}
		});
		ListenableFuture<SendResult<String, String>> future2 = kafkaTemplateClient.send(recordMasterDb1);
		future2.addCallback(new ListenableFutureCallback<SendResult<String, String>>() {
			@Override
			public void onSuccess(SendResult<String, String> result) {
				logger.info("sent message = {} with offset = {}", result, result.getRecordMetadata().offset());
			}

			@Override
			public void onFailure(Throwable throwable) {
				logger.error("unable to send message= {}", Arrays.toString(mqttMessage.getPayload()));
			}
		});

		ListenableFuture<SendResult<String, String>> future3 = kafkaTemplateClient.send(recordMasterDb2);
		future3.addCallback(new ListenableFutureCallback<SendResult<String, String>>() {
			@Override
			public void onSuccess(SendResult<String, String> result) {
				logger.info("sent message = {} with offset = {}", result, result.getRecordMetadata().offset());
			}

			@Override
			public void onFailure(Throwable throwable) {
				logger.error("unable to send message= {}", Arrays.toString(mqttMessage.getPayload()));
			}
		});


		ListenableFuture<SendResult<String, String>> future4 = kafkaTemplateClient.send(recordMasterDb3);
		future4.addCallback(new ListenableFutureCallback<SendResult<String, String>>() {
			@Override
			public void onSuccess(SendResult<String, String> result) {
				logger.info("sent message = {} with offset = {}", result, result.getRecordMetadata().offset());
			}

			@Override
			public void onFailure(Throwable throwable) {
				logger.error("unable to send message= {}", Arrays.toString(mqttMessage.getPayload()));
			}
		});
		ListenableFuture<SendResult<String, String>> future5 = kafkaTemplateClient.send(recordMasterDb4);
		future5.addCallback(new ListenableFutureCallback<SendResult<String, String>>() {
			@Override
			public void onSuccess(SendResult<String, String> result) {
				logger.info("sent message = {} with offset = {}", result, result.getRecordMetadata().offset());
			}

			@Override
			public void onFailure(Throwable throwable) {
				logger.error("unable to send message= {}", Arrays.toString(mqttMessage.getPayload()));
			}
		});

		future1.addCallback(new ListenableFutureCallback<SendResult<String, String>>() {

			@Override
			public void onSuccess(SendResult<String, String> result) {
				logger.info("sent message = {} with offset = {}", result, result.getRecordMetadata().offset());
			}

			@Override
			public void onFailure(Throwable throwable) {
				logger.error("unable to send message= {}", Arrays.toString(mqttMessage.getPayload()));
			}
		});

	}

	@Override
	public void deliveryComplete(IMqttDeliveryToken token) {

	}

//	@Override
//	protected void config(String broker, Integer port, Boolean ssl, Boolean withUserNamePass) {
//		logger.info("Inside Parameter Config");
//		//String protocal = this.TCP;
//
//		//this.brokerUrl = protocal + this.broker + colon + port;
//		this.persistence = new MemoryPersistence();
//		this.connectionOptions = new MqttConnectOptions();
//
//		try {
//			this.mqttClient = new MqttClient(brokerUrl, clientId, persistence);
//			this.connectionOptions.setCleanSession(true);
////			this.connectionOptions.setPassword(this.password.toCharArray());
////			this.connectionOptions.setUserName(this.userName);
//			this.mqttClient.connect(this.connectionOptions);
//			this.mqttClient.setCallback(this);
//		} catch (MqttException me) {
//			throw new com.bellurbis.exception.MqttException("Not Connected");
//		}
//	}

	@Override
	protected void config() {
		logger.info("Inside Config with parameter");
		//this.brokerUrl = this.TCP + this.broker + colon + this.port;
		this.brokerUrl =this.broker;
		this.persistence = new MemoryPersistence();
		this.connectionOptions = new MqttConnectOptions();
		try {
			this.mqttClient = new MqttClient(brokerUrl, clientId, persistence);
			this.connectionOptions.setCleanSession(true);
//			this.connectionOptions.setPassword(this.password.toCharArray());
//			this.connectionOptions.setUserName(this.userName);
			this.mqttClient.connect(this.connectionOptions);
			this.mqttClient.setCallback(this);
		} catch (MqttException me) {
			throw new com.bellurbis.exception.MqttException("Not Connected");
		}
	}

	/*
	 * subscribeMessage is used for subscribe the topic from the client side
	 *
	 */ @Override
	public void subscribeMessage(String topic) {
		try {
			this.mqttClient.subscribe(topic, this.qos);
		} catch (MqttException me) {
			if (retryAttempt < 10) {
				logger.error("Not able to Read Topic ={}", topic);
				retryAttempt++;
			}
			this.config();
		}
	}

	@Override
	public void disconnect() {
		try {
			this.mqttClient.disconnect();
		} catch (MqttException me) {
			logger.error("ERROR", me);
		}
	}
}
